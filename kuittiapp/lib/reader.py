''' 
Tools for scanning and reading the receipt.
'''
import concurrent.futures
import time

import cv2
import numpy as np
import pytesseract
from PIL import Image
from skimage.filters import threshold_local

def scan_image(file_path):
    '''
    Method for scanning the receipt.
    Finds the receipt in the picture, crops and executes the four point transformation to fix the perspective and scale of the receipt.
  
    Raises an error if no receipt is found in the picture.

    Parameters 
    ----------
    file_path 
        Path to the image of the receipt.

    Returns
    -------
    warped
        Image of cropped and transformed receipt.
    '''
    # read file and take a copy of an original picture
    print('[receipt] Kuitti: {}'.format(file_path))
    image = cv2.imread(file_path)
    original_image = image.copy()
    # define scale ratio and modify the receipt
    RATIO = 17
    modified_image = modify_image(image, ratio = 1/RATIO, gray = 1, blur_filter = 0, blur_gaus = 1, treshold = 0, inverse = 0)
    # find edges, contours and sort contours by contour area and pick 5 largest
    edged_image = cv2.Canny(modified_image, 75, 200)
    contours = cv2.findContours(edged_image.copy(), cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)[1]
    contours = sorted(contours, key=cv2.contourArea, reverse=True)[:5]
    # loop contours
    for cnt in contours:
        # approximate polygon, curve closed True
        epsilon = 0.01 * cv2.arcLength(cnt, True)
        approx = cv2.approxPolyDP(cnt, epsilon, True)
        print('[receipt] Kulmia: {} kpl'.format(len(approx)))
        # if 4 corners
        if len(approx) == 4:
            # this is the contour we where looking for i.e. this is the receipt
            # reshape contour
            # do perspective transformation to original image
            screenCnt = approx
            pts = order_points(screenCnt.reshape(4, 2) * RATIO)
            warped = four_point_transform(original_image, pts)
            break
    # if transformation succeeded - return cropped picture
    # else - raise error
    if 'warped' in locals() or 'warped' in globals():
        return warped
    else:
        raise RuntimeError('Skanneri ei tunnistanut kuvasta kuittia.')

def modify_image(image, ratio, gray = 1, blur_filter = 1, blur_gaus = 0, treshold = 1, inverse = 0):
    '''
    Generic image modifying method.

    Parameters 
    ----------
    image
        Image of the receipt.
    ratio
        Image scaling ratio.
    gary
        Grayscale indicator.
    blur_filter
        Blur bilateral indicator.
    blur_gaus
        Blur Gaussian indicator.
    treshold
        Treshold indicator.
    inverse
        Inverse indicator.

    Returns
    -------
    image
        Modified image.
    '''
    # resize
    if ratio != 1:
        image = cv2.resize(image,None,fx=ratio, fy=ratio, interpolation = cv2.INTER_CUBIC)
    # grayscale
    if gray == 1:
        image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    # Blur bilateralFilter
    if blur_filter == 1:
        image = cv2.bilateralFilter(image,9,20,2)
    # Blur Gaussian
    if blur_gaus == 1:
        image = cv2.GaussianBlur(image, (5, 5), 0)
    # Treshold
    if treshold == 1:
        T = threshold_local(image, 71, offset = 15, method = "gaussian")
        image = (image > T).astype("uint8") * 255
    # Inverse
    if inverse == 1:
        image = cv2.threshold(image, 180, 255, cv2.THRESH_BINARY_INV)[1]
    # return
    return image

# https://www.pyimagesearch.com/2014/08/25/4-point-opencv-getperspective-transform-example/
def order_points(pts):
	# initialzie a list of coordinates that will be ordered
	# such that the first entry in the list is the top-left,
	# the second entry is the top-right, the third is the
	# bottom-right, and the fourth is the bottom-left
	rect = np.zeros((4, 2), dtype = "float32")
 
	# the top-left point will have the smallest sum, whereas
	# the bottom-right point will have the largest sum
	s = pts.sum(axis = 1)
	rect[0] = pts[np.argmin(s)]
	rect[2] = pts[np.argmax(s)]
 
	# now, compute the difference between the points, the
	# top-right point will have the smallest difference,
	# whereas the bottom-left will have the largest difference
	diff = np.diff(pts, axis = 1)
	rect[1] = pts[np.argmin(diff)]
	rect[3] = pts[np.argmax(diff)]
 
	# return the ordered coordinates
	return rect

# http://www.pyimagesearch.com/2014/08/25/4-point-opencv-getperspective-transform-example/
def four_point_transform(image, rect):
    # obtain a consistent order of the points and unpack them
    # individually
    rect = np.array(rect,np.float32)
    (tl, tr, br, bl) = rect
 
    # compute the width of the new image, which will be the
    # maximum distance between bottom-right and bottom-left
    # x-coordiates or the top-right and top-left x-coordinates
    widthA = np.sqrt(((br[0] - bl[0]) ** 2) + ((br[1] - bl[1]) ** 2))
    widthB = np.sqrt(((tr[0] - tl[0]) ** 2) + ((tr[1] - tl[1]) ** 2))
    maxWidth = max(int(widthA), int(widthB))
 
    # compute the height of the new image, which will be the
    # maximum distance between the top-right and bottom-right
    # y-coordinates or the top-left and bottom-left y-coordinates
    heightA = np.sqrt(((tr[0] - br[0]) ** 2) + ((tr[1] - br[1]) ** 2))
    heightB = np.sqrt(((tl[0] - bl[0]) ** 2) + ((tl[1] - bl[1]) ** 2))
    maxHeight = max(int(heightA), int(heightB))
 
    # now that we have the dimensions of the new image, construct
    # the set of destination points to obtain a "birds eye view",
    # (i.e. top-down view) of the image, again specifying points
    # in the top-left, top-right, bottom-right, and bottom-left
    # order
    dst = np.array([
        [0, 0],
        [maxWidth - 1, 0],
        [maxWidth - 1, maxHeight - 1],
        [0, maxHeight - 1]], dtype = "float32")
 
    # compute the perspective transform matrix and then apply it
    M = cv2.getPerspectiveTransform(rect, dst)
    warped = cv2.warpPerspective(image, M, (maxWidth, maxHeight))
 
    # return the warped image
    return warped

def detect_words(image, store_name):
    '''
    Method to recognize the text parts of the receipt. The confining of the words is done according to given store name. 
    If no store is given, default value is used in the confinig.

    Parameters 
    ----------
    image
        Image of the receipt
    store_name
        Store code ('S', 'K', 'L')
    
    Returns
    -------
    sorted_word_unit_list
        Sorted list of text parts as a list of word_unit units.
    '''
    # Modify receipt
    inverse_image = modify_image(image, ratio= 1, gray = 0, blur_filter = 0, blur_gaus = 0, treshold = 0, inverse = 1)
    #====== Define cropping according to store ====================
    #      - MORPH_RECT = kernel is rectangle
    #      - (x,y) the bigger the number the bigger the dilation while iterating
    if store_name == 'ERROR':
        raise ValueError('Kauppaa ei kyetty tunnistamaan.')
    # S-ryhma
    elif store_name == 'S':
        dilate_kernel_size = (4,1)
        dilate_iterate = 9
        erode_kernel_size = (2,1)
        erode_iterate = 2
    # K-ryhma
    elif store_name == 'K':
        dilate_kernel_size = (6,1)
        dilate_iterate = 7
        erode_kernel_size = (3,1)
        erode_iterate = 3
    # Lidl
    elif store_name == 'L':
        dilate_kernel_size = (5,2)
        dilate_iterate = 9
        erode_kernel_size = (2,1)
        erode_iterate = 3
    # Default
    else:
        dilate_kernel_size = (4,1)
        dilate_iterate = 9
        erode_kernel_size = (2,1)
        erode_iterate = 2
    # structuring elements
    dilate_kernel = cv2.getStructuringElement(cv2.MORPH_RECT , dilate_kernel_size)
    dilated = cv2.dilate(inverse_image, dilate_kernel, iterations=dilate_iterate)
    erode_kernel = cv2.getStructuringElement(cv2.MORPH_RECT , erode_kernel_size)
    eroded = cv2.erode(dilated, erode_kernel, iterations=erode_iterate)
    # contours
    contours = cv2.findContours(eroded,cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)[1]        
    # ====== LOOP CONTOURS =====================================================
    # init word_unit_list
    word_unit_list = []
    # loop contours
    for contour in contours:
        # coordinates and size
        [x, y, w, h] = cv2.boundingRect(contour)
        # ignore contours which are too small or lay in the boundary of the receipt
        if (w < 45 and h < 35) or x == 0 or y == 0 or x + w == np.size(image, 1) or y + h == np.size(image, 0):
            continue
        # init word_unit and append word_unit to word_unit_list
        new_word_unit = word_unit(x,y,w,h,0,False,False,'')
        word_unit_list.append(new_word_unit)
    # ====== SORT PALAT AND CONCLUDE ROWS ======================================
    # sort word_unit_list by y coordinate
    sorted_word_unit_list = sorted(word_unit_list, key=lambda word_unit: word_unit.y)
    # define row for word_unit - if the difference of y coordinates of word_unit and previous word_unit is > 30, then they are on separate rows
    sorted_word_unit_list[0].row = 1
    for i in range(1,len(sorted_word_unit_list)):
        if abs(sorted_word_unit_list[i].y - sorted_word_unit_list[i-1].y) > 30:
            sorted_word_unit_list[i].row = sorted_word_unit_list[i-1].row + 1    
        else:
            sorted_word_unit_list[i].row = sorted_word_unit_list[i-1].row
    # sort word_unit_list by row and x coordinate
    sorted_word_unit_list = sorted(sorted_word_unit_list, key=lambda word_unit: (word_unit.row, word_unit.x))
    # return
    return sorted_word_unit_list

def find_separator_row(word_unit_list):
    '''
    Method to find the first separator line of the receipt '------'. 
    Separator line is normally located at far right of receipt. It's height is small and length is big. 
    Therefore, we search separator line with conditions:
        - word_unit.x > 100
        - word_unit.h < 35
        - word_unit.y > 200

    Parameters
    ----------
    word_unit_list
        List of text areas as a list of word_unit units.

    Returns
    -------
    separator_row
        Row number of the first separator line.
    '''
    for word_unit in word_unit_list:
        if word_unit.x > 200 and word_unit.h < 35 and word_unit.w > 200:
            separator_row = word_unit.row
            return separator_row
    # if not found return default = 30
    return 30

def recognize_store(tesseract_path, image, word_unit_list):
    '''
    Method to recognize the store.
    The first ten word units are fed into Tesseract. If text of word unit matches any of the keys of the store_name_dict, set store name and return.
    Else return store name 'ERROR'.

    Parameters 
    ----------
    tesseract_path
        Tesseract OCR path.
    image
        Image of the receipt.
    word_unit_list
        List of text areas as a list of word_unit units.

    Returns
    -------
    store_name
        The code for recognized store (S, K or L). Code is 'ERROR' if no store is recognized.
    '''
    store_name = 'ERROR'
    store_name_dict = {'ALEPA' : 'S', 'PLEPA' : 'S', 'FLEPA' : 'S', 'MEPA' : 'S', 'SOKOS' : 'S', 'S-MARKET' : 'S','S—MARKET' : 'S','S-HARKET' : 'S', 'S-MFRKET' : 'S','S-MPRKET' : 'S', 'PRISMA' : 'S',
                    'PRISHA' : 'S','PHISMA' : 'S','PRISHR' : 'S', 'OSUUSKAUPPA' : 'S', 'ALEFA': 'S', 'K-Market' : 'K','K—Market' : 'K', 'K—market' : 'K',"K—Mar'ket" : 'K','K-market' : 'K','K-Harket' : 'K',
                    'K-CITYMARKET' : 'K','K-SUPERMARKET' : 'K','K—SUPERMARKET' : 'K', 'K-Markgt': 'K', 'K—Supermarket':'K' , 'K-supermarket' : 'K', "K-Marke't" : 'K', 'Lidl' : 'L','Lid]' : 'L','L1d1' : 'L','Lidi' : 'L'}
    sub_word_unit_list = word_unit_list[:10]
    for word_unit in sub_word_unit_list:
        read_with_tesseract(tesseract_path, image, 100, word_unit)
        if any(word_unit.text == store_name for store_name in store_name_dict.keys()):
            store_name = store_name_dict[word_unit.text]
            break
    print('[receipt] Tunnistettu kauppa: %s' % store_name)
    return store_name

def threaded_read_with_tesseract(tesseract_path, modified_image, separator_row, word_unit_list):
    '''
    Calls Tesseract OCR with threads.

    Parameters
    ----------
    tesseract_path
        Tesseract OCR path.
    modified_image
        Modified image of the receipt.
    separator_row
        Row number of first separator line of the receipt.
    word_unit_list
        List of word units of the recept.
    '''
    t0 = time.monotonic()
    with concurrent.futures.ThreadPoolExecutor(max_workers=4) as executor:
        futures = {executor.submit(read_with_tesseract, tesseract_path, modified_image, separator_row, unit) for unit in word_unit_list}
        concurrent.futures.wait(futures)
        print("[receipt] Processed: %s seconds" % (time.monotonic()-t0))

def read_with_tesseract(tesseract_path, image, separator_row, word_unit):
    '''
    Method that feeds the text parts of the receipt into Tesseract OCR and updates the 'text' attribute of word_unit.

    Parameters
    ----------
    tesseract_path
        Tesseract OCR path.
    image
        Image of the receipt.
    separator_row
        Row number of first separator line of the receipt.
    word_unit
        Word unit.
    '''
    pytesseract.pytesseract.tesseract_cmd = tesseract_path
    # width and letter height. Letter height is typically 40-60px
    width = np.size(image, 1)
    letter_height = 30
    # feed rows till first separator line + 1 into Tesseract
    if word_unit.row < (separator_row + 2):
        cropped = image[word_unit.y : word_unit.y +  word_unit.h , word_unit.x : word_unit.x + word_unit.w]
        # if the height of word_unit is less than letter height - don't read
        if word_unit.h < letter_height:
            word_unit.text = ''
        # the last third of the receipt is numbers
        elif word_unit.x > (width-(width/3)):
            # psm 8 = one word per picture, digits = only numbers and specal characters
            word_unit.text =  pytesseract.image_to_string(Image.fromarray(cropped), lang="fin", config= '--psm 8 digits')
        else:
            # psm 8 = one word per picture, bazaar = use your own word list
            word_unit.text =  pytesseract.image_to_string(Image.fromarray(cropped), lang="fin", config= '--psm 8 bazaar')
    # tsek
    word_unit.print_word_unit()

class word_unit:
    '''
    Class for word unit. This is used in the receipt reading methods.
    '''
    def __init__(self, xcor, ycor, width, height, row, firstofrow, lastofrow, text):
        self.x = xcor
        self.y = ycor
        self.w = width
        self.h = height
        self.row = row
        self.first_of_row = firstofrow
        self.last_of_row = lastofrow
        self.text = text
    
    def print_word_unit(self):
        print('[receipt] Row: %s (%s, %s) (%s, %s) text: %s' % ( self.row, self.x, self.y, self.w, self.h, self.text))

