import datetime as dtime
import re

class product:
    '''
    Class for product unit.
    '''
    def __init__(self, date='20000101', year=2000, month=1, day=1, 
                    product_class='', product_name='', unit_weight=0, unit_price=0, units=1, total_weight=0, total_price=0, price_per_weight=0, 
                        store=None, buyer=None):
        self.date = date
        self.year = year
        self.month = month
        self.day = day
        self.product_class = product_class
        self.product_name = product_name
        self.unit_weight = unit_weight
        self.unit_price = unit_price
        self.units = units
        self.total_weight = total_weight
        self.total_price = total_price
        self.price_per_weight = price_per_weight
        self.store = store
        self.buyer = buyer
    
    def count_total_weight(self):
        '''
        Count the total weight.
        '''
        try:
            self.units = int(self.units)
            self.unit_weight = float(self.unit_weight)
            if self.units > 0 and self.unit_weight > 0:
                self.total_weight = round(self.units*self.unit_weight,3)
        except ValueError:
            pass
    
    def count_price_per_weight(self):
        '''
        Count the price per weight.
        '''
        try:
            self.unit_weight = float(self.unit_weight)
            self.unit_price = float(self.unit_price)
            if self.unit_weight > 0 and self.unit_price > 0:
                self.price_per_weight = round(self.unit_price/self.unit_weight,3)
        except ValueError:
            pass
            
    def update_unit_price(self):
        '''
        Update the unit price.
        '''
        try:
            self.units = int(self.units)
            self.total_price = float(self.total_price)
            if self.units == 1:
                self.unit_price = self.total_price
            if self.units > 1:
                self.unit_price = round(self.total_price/self.units,3)
        except ValueError:
            pass

    def update_purchase_date(self):
        '''
        Update the purchase date from the attribute 'date'.
        Preferred date format is YYYYMMDD.
        '''
        # replace all symbols and special characters with '-'
        self.date = re.sub('[^0-9]+', '-', self.date)
        # format dd-mm-yyyy or d-mm-yyyy or dd-m-yyyy
        try:
            stripped_date = dtime.datetime.strptime(self.date, '%d-%m-%Y')
        except ValueError:
            pass
        # format ddmm-yyyy or dmm-yyyy or ddm-yyyy
        try:
            stripped_date = dtime.datetime.strptime(self.date, '%d%m-%Y')
        except ValueError:
            pass
        # format dd-mmyyyy or d-mmyyyy or dd-myyyy
        try:
            stripped_date = dtime.datetime.strptime(self.date, '%d-%m%Y')
        except ValueError:
            pass
        # format ddmmyyyy or dmmyyyy or ddmyyyy
        try:
            stripped_date = dtime.datetime.strptime(self.date, '%d%m%Y')
        except ValueError:
            pass
        # fomat YYYY-mm-dd (ostos-page form)
        try:
            stripped_date = dtime.datetime.strptime(self.date, '%Y-%m-%d')
        except ValueError:
            pass
        # format YYYYmmdd
        try:
            stripped_date = dtime.datetime.strptime(self.date, '%Y%m%d')
        except ValueError:
            pass
        if 'stripped_date' in locals() or 'stripped_date' in globals():
            self.day = int(stripped_date.day)
            self.month = int(stripped_date.month)
            self.year = int(stripped_date.year)
            self.date = stripped_date.strftime('%Y%m%d')

    def clean_product(self):
        '''
        Clean the product by making sure that the string attributes have only ascii characters in them 
        and numerical attributes are either int or float.
        '''
        # STRINGS
        strings = ['date', 'product_class', 'product_name', 'store', 'buyer']
        for st in strings:
            setattr(self, st, re.sub("[ä]", 'a', getattr(self, st)))
            setattr(self, st, re.sub("[Ä]", 'A', getattr(self, st)))
            setattr(self, st, re.sub("[ö]", 'o', getattr(self, st)))
            setattr(self, st, re.sub("[Ö]", 'O', getattr(self, st)))
            setattr(self, st, re.sub(r'[^\x00-\x7F]', '', getattr(self, st))) # only ascii
        # YEAR
        try:
            self.year = int(self.year)
        except ValueError:
            self.year = 2000
        # MONTH
        try:
            self.month = int(self.month)
        except ValueError:
            self.month = 1
        # DAY
        try:
            self.day = int(self.day)
        except ValueError:
            self.day = 1
        # unit weight
        try: 
            self.unit_weight = float(self.unit_weight)
        except ValueError:
            self.unit_weight = 0
        # unit price
        try:
            self.unit_price = float(self.unit_price)
        except ValueError:
            self.unit_price = 0
        # unit count
        try:
            self.units = int(self.units)
        except ValueError:
            self.units = 0
        # total weight
        try:
            self.total_weight = float(self.total_weight)
        except ValueError:
            self.total_weight = 0
        # total price
        try:
            self.total_price = float(self.total_price)
        except ValueError:
            self.total_price = 0
        # price per weight
        try:
            self.price_per_weight = float(self.price_per_weight)
        except ValueError:
            self.price_per_weight = 0

    def product_to_dict(self):
        '''
        Calls clean_product and modifies the product into a dictionary.
        '''
        self.clean_product()
        return_dict = {attribute : getattr(self,attribute) for attribute in dir(self) if not attribute.startswith('__') and not callable(getattr(self,attribute))}
        return return_dict

    @classmethod
    def dict_to_product(cls, product_dict):
        '''
        Creates a new class instance from dictionary and calls clean_product.
        '''
        unit = cls(
            product_dict['date'], 
            product_dict['year'], 
            product_dict['month'], 
            product_dict['day'],
            product_dict['product_class'],
            product_dict['product_name'],
            product_dict['unit_weight'],
            product_dict['unit_price'],
            product_dict['units'],
            product_dict['total_weight'],
            product_dict['total_price'],
            product_dict['price_per_weight'],
            product_dict['store'],
            product_dict['buyer'])
        # clean unit
        unit.clean_product()
        # return
        return unit