'''
Tools for backup and archive.
'''
import datetime
import os
import shutil
from subprocess import PIPE, Popen
from urllib import parse

import onedrivesdk
import requests

def create_database_backup(config):
    '''
    Create backup of the database. The backup is created calling executable mysqldump.exe.

    Parameters
    ----------
    config
        Application configs.

    Returns
    -------
    backup_path
        Full path of the created backup file.
    '''
    # define backup file name, construct create backup command and execute with sub process
    connection = config['CONNECTION']
    backup_location = config['DATABASEBACKUP']
    now = datetime.datetime.now()
    times = 'database_' + config['ENV'] + '_' + now.strftime("%Y%m%d") + '.sql'
    backup_path = os.path.join(backup_location, times)
    cmd = '"{}" -u {} -p{} {}'.format(config['DATABASEDUMP'], connection['UID'], connection['PASSWORD'], connection['DATABASE'])
    p = Popen(cmd, shell=True, stdout=PIPE, stderr=PIPE)
    # save results and return full path
    with open(backup_path, "wb") as f:
        f.writelines(p.stdout)
    return backup_path

def archive_image(file_path, archive_path):
    '''
    Move receipt to archive folder.
    '''
    try:
        file_name = os.path.basename(file_path)
        file_archive_path = os.path.join(archive_folder, file_name)
        shutil.move(file_path, file_archive_path)
    except Exception as err:
        print('[error] Kuitin arkistointi epäonnistui: {}'.format(err.args))

def backup_to_drive(config, backup_list):
    '''
    Create connection to OneDrive, authenticate and upload all files in backup_file_list to drive.

    Parameters
    ----------
    config
        Application configs.
    backup_list
        List of tuples containing full path of the file and target folder id.
    '''
    od_client = onedrive(config)
    od_client.authenticate()
    for item in backup_list:
        od_client.upload(item[0], item[1])

class onedrive:
    '''
    Class for OneDrive connection. Connection is build with onedrivesdk.
    '''
    def __init__(self, config):
        '''
        Init

        Parameters
        ----------
        config
            Application configs.
        '''
        self.redirect_uri = config['OD_REDIRECT_URI']
        self.client_secret = config['OD_CLIENT_SECRET']
        self.client_id = config['OD_CLIENT_ID']
        self.api_base_url = config['OD_API_URL']
        self.scopes = config['OD_SCOPES']
        self.headers = config['OD_HEADERS']
        self.loop = 'YADDA'
        self.client = None

    def authenticate(self):
        '''
        Authenticate and get code.

        If loop-parameter is not provided for methods AuthProvider and OneDriveClient, they will try to get event loop with asyncio.get_event_loop().
        I don't want that >:( 
        '''
        http_provider = onedrivesdk.HttpProvider()
        auth_provider = onedrivesdk.AuthProvider(http_provider=http_provider, client_id=self.client_id, scopes=self.scopes, loop=self.loop)
        self.client = onedrivesdk.OneDriveClient(self.api_base_url, auth_provider, http_provider, loop=self.loop)
        auth_url = self.client.auth_provider.get_auth_url(self.redirect_uri)
        r = requests.get(auth_url, headers = self.headers )
        params = parse.parse_qs(parse.urlsplit(r.url).query)
        self.client.auth_provider.authenticate(params['code'], self.redirect_uri, self.client_secret)

    def upload(self, file_path, folder_id):
        '''
        Uploads a file to OneDrive. 
        The file will have the same name and it will be uploaded to a given folder (folder_id).

        Parameters
        ----------
        file_path
            Full path of file.
        folder_id
            Id of the OneDrive folder, where the file will be uploaded.
        '''
        file_name = os.path.basename(file_path)
        returned_item = self.client.item(drive='me', id=folder_id).children[file_name].upload(file_path)
        return returned_item
