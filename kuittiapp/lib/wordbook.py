'''
Tools for Tesseract wordlist, fixing typos and missreads of OCR tool and product name logger.
'''
import collections as coll
import itertools
import operator
import re
import os
import json
import datetime

def update_word_list(product_list, word_list_path):
    '''
    Method for updating custom word list (user_words) of Tesseract OCR.
    Adds new words to list, removes doubles and keeps the list in alphabetical order.

    Parameters
    ----------
    product_list
        List of product units.
    word_list_path
        Path to Tesseract word list.
    
    Returns
    -------
    added_words
        Number of words added to word list.
    '''
    # init added words
    added_words = 0
    with open(word_list_path,'r+') as word_list:
        # Read the file into a list and truncate content
        read_list = word_list.read().splitlines()
        word_list.truncate(word_list.seek(0))
        # Count occurences and get a list of sorted unique elements
        count = coll.Counter(read_list)
        elements = list(sorted(count))
        # Input words
        for product in product_list:
            product_words = product.product_name.split(' ')
            for word in product_words:
                # If word exists in list - continue
                if any(element == word for element in elements):
                    continue
                # Else add word to list
                else:
                    elements.append(word)
                    added_words += 1
        # Write new word list to file
        for element in elements:
            word_list.write("%s\n" % element)
    return added_words

def fix_product_names(product_list, average_info, word_list_path):
    '''
    Method for fixing typos and missread words by Tesseract.

    Parameters
    ----------
    product_list
        List of product units.
    average_info
        List of average product info from database.
    word_list_path
        Path to Tesseract word list.
    '''
    fix_single_words(product_list, word_list_path)
    fix_whole_name(product_list, average_info)

def fix_single_words(product_list, word_list_path):
    '''
    Method for fixing individual words.

    Parameters
    ----------
    product_list
        List of product units.
    word_list_path
        Path to Tesseract word list.
    '''
    with open(word_list_path,'r') as word_list:
        # Read the file into a list
        elements = word_list.read().splitlines()
    # loop product_list
    for product in product_list:
        # split product name f.ex 'MAUSTAMATON JUGURTTI' has to be divided.
        name_parts = product.product_name.split(' ')
        # init fixed word
        fixed_part = []
        for name_part in name_parts:
            # if no word - continue
            if len(name_part) == 0:
                continue
            # filter word list by string length
            loop_elements = [x for x in elements if abs( len(x)-len(name_part) ) < 5]
            # Get letters of the string we want to fix. Pick the most common letters and the first letter of the word
            # Filter loop_elements such that the elements contain all the accepted letters
            cleaned_part = re.sub("[^aeijklnoprstuvyAEIJKLNOPRSTUVY]", "", name_part)
            letters = list(set(cleaned_part + name_part[0]))
            for letter in letters:
                loop_elements = [x for x in loop_elements if letter in x]
            # find fix if we have elements to loop
            if len(loop_elements) > 0:
                # comparison results
                results = []
                for x in loop_elements:
                    # length difference
                    len_diff = len(x) - len(name_part)
                    # compare the beginning of words
                    pair = zip(name_part, x)
                    incorrect1 = len([c for c,d in pair if c!=d]) 
                    # compare the end of words
                    if len_diff == 0:
                        incorrect2 = 0
                    elif len_diff > 0:
                        alter_part = ('A' * len_diff) + name_part
                        pair = zip(alter_part, x)
                        incorrect2 = len([c for c,d in pair if c!=d]) - len_diff
                    else:
                        alter_x = ('A' * abs(len_diff)) + x
                        pair = zip(name_part, alter_x)
                        incorrect2 = len([c for c,d in pair if c!=d]) - len_diff
                    # define object function
                    object_function = len_diff + incorrect1 + incorrect2
                    # add to results
                    results.append([x, object_function])
                # pick the one with lowest score
                word, value = min(results, key = operator.itemgetter(1))
                fixed_part.append(word)
            # if there is no loop_elements - purpose no fix
            else: 
                fixed_part.append(name_part)
        # update product name
        if len(fixed_part) > 0:
            product.product_name = (" ".join(str(x) for x in fixed_part))
        else:
            pass

def fix_whole_name(product_list, average_info):
    '''
    Method for fixing the whole product name. The correct product name is searched from the database. 
    Notice that database.average_info() must be loaded!
    
    Parameters
    ----------
    product_list
        List of product units.
    average_info
        List of average product info from database.
    '''
    # Get list of unique products with average info
    products_with_average_info = [x['product_name'] for x in average_info]
    products_with_average_info = coll.Counter(products_with_average_info)
    products_with_average_info = list(sorted(products_with_average_info))
    # loop product_list
    for product in product_list:
        result_set = []
        # create compare_product_list - filter products_with_average_info according to split count
        # if only one word - move to next product
        if len(product.product_name.split(' ')) == 1:
            continue
        # if product_name has less or equal than three words - one or less words are allowed to diff
        elif len(product.product_name.split(' ')) <= 3:
            compare_product_list = [x for x in products_with_average_info if abs (len(x.split(' ')) - len(product.product_name.split(' '))) <= 1]
        # if product_name has more than 3 words - two or less words are allowed to diff
        else:
            compare_product_list = [x for x in products_with_average_info if abs (len(x.split(' ')) - len(product.product_name.split(' '))) <= 2]
        # split product_name
        product_parts = product.product_name.split(' ')
        # if compare_product_list is not empty - try to find a fix
        if len(compare_product_list) > 0:
            # loop
            for compare_product in compare_product_list:
                # split compare_product
                compare_parts = compare_product.split(' ')
                # len diff
                len_diff = len(product_parts) - len(compare_parts)
                #=========== INIT ========================
                # init incorrect words
                incorrect_words = abs(len_diff)
                # same number of words
                if len_diff == 0:
                    le = len(compare_parts)
                    incorrect_letters = 0
                elif len_diff > 0:
                    le = len(compare_parts)
                    incorrect_letters = sum( [ len(word) for word in product_parts[le:] ] ) 
                else:
                    le = len(product_parts)
                    incorrect_letters = sum( [ len(word) for word in compare_parts[le:] ] ) 
                # match
                for i in range(le):
                    # match from the beginning
                    pair = itertools.zip_longest(product_parts[i],compare_parts[i])
                    # letter diff
                    letter_diff = len([c for c,d in pair if c!=d])
                    # count letter diff
                    incorrect_letters = incorrect_letters + letter_diff
                    # count word diff
                    if letter_diff > 0:
                        incorrect_words = incorrect_words + 1 
                # add to result set
                result_set.append([compare_product, incorrect_words, incorrect_letters])
            # find the one with best match
            result_sorted = sorted(result_set, key = lambda result: (result[1], result[2]))
            compare_product_match = result_sorted[0][0]
            # insert
            product.product_name = compare_product_match
        # if compare_product_list is empty - don't try to fix, continue
        else:
            continue

def write_product_log(product_list, log_file_path, state=0):
    '''
    Product name log writer. Writes log of product names when there're first read and after they've been fixed by app or user.
    Creates file if it doesn't exist.

    Parameters
    ----------
    product_list
        List of products as product units.
    log_file_path
        Path to log file.
    state
        State indicator. 0 = before product name correction and 1 = after product name correction.
    '''
    if len(log_file_path) > 0:
        if state == 0:
            # State 0: create timestamp, init and create product list and write to file
            stamp = datetime.datetime.today().strftime('%Y%m%d%H%M%S')
            products = {}
            for index, product in enumerate(product_list):
                products[index] = {'before' : product.product_name}
            # check if file exists
            if not os.path.isfile(log_file_path):
                with open(log_file_path, "w+",encoding='utf-8') as log:
                    data = [{}]
                    json.dump(data, log)
            # write
            with open(log_file_path, "r+",encoding='utf-8') as log:
                data = json.load(log)
                data[0][stamp] = products
                log.seek(0)
                json.dump(data, log)
                log.truncate()
        else:
            # State 1: Fill product list that was created in state 0
            with open(log_file_path, "r+",encoding='utf-8') as log:
                data = json.load(log)
                # find the biggest key
                max_key = max(data[0].keys())
                # write to right row 
                # OBS - if rows removed during päivitys - this won't work
                # I rarely remove rows so whatever
                for index, product in enumerate(product_list):
                    try:
                        data[0][max_key][str(index)]['after'] = product.product_name
                    except:
                        pass
                log.seek(0)
                json.dump(data, log)
                log.truncate()