'''
Tools for parsing the information of receipt.
'''
import operator
import re
from . import product

def replace_scandinavian(strn):
    '''
    Stupid scandinavian characters.
    '''
    tab = dict.fromkeys('Ä', 'A')
    tab.update({'ä' : 'a', 'Å' : 'A', 'å' : 'a', 'Ö' : 'O', 'ö' : 'o'})
    return strn.translate(str.maketrans(tab))

def parse_receipt(word_unit_list, separator_row, store_name):
    '''
    Method to parse receipt given the list of text areas, separator line and store name.
    First method will deduct the first and the last word of every row of the receipt.
    Second method will call store specific parser.

    Parameters
    ----------
    word_unit_list
        The list of text parts as a list of word_unit units.
    separator_row
        Row number of separator line.
    store_name
        Recognized store name.
            
    Returns
    -------
    product_list 
        List of parsed products as a list of product units.
    check_sum
        Check sum of the receipt.
    '''
    # Deduct the first and the last word_unit of the row
    for i in range(1,len(word_unit_list)):
        if word_unit_list[i].row > word_unit_list[i-1].row:
            word_unit_list[i].first_of_row = True
            word_unit_list[i-1].last_of_row = True
    # call store parser
    if store_name in ['S', 'K']:
        product_list, check_sum = parse_receipt_S_K(word_unit_list, separator_row)
    elif store_name in ['L']:
        product_list, check_sum = parse_receipt_L(word_unit_list, separator_row)
    else:
        raise RuntimeError('Kauppaa ei määritelty')
    # return
    return product_list, check_sum

def parse_receipt_S_K(word_unit_list, separator_row):
    '''
    Parser for S-ryhmä and K-ryhmä receipts.

    Parameters
    ----------
    word_unit_list
        The list of text parts as a list of word_units.
    separator_row
        Row number of separator line.
            
    Returns
    -------
    product_list 
        List of parsed products as a list of product units.
    check_sum
        Check sum of the receipt.
    '''
    product_list = []
    check_sum = 0
    # Store name is at the beginning of the receipt
    full_store_name = word_unit_list[0].text + ' ' + word_unit_list[1].text
    full_store_name = re.sub("[.,;:]", '', full_store_name)
    # ======================= concluding the beginning_row ===============================
    # beginning_row is the last row before the listing of purchased products.
    # The product listing starts after the purchase date. The purchase date 
    #     - can be found in the beginning of the receipt (word_unit.row between 2 and first separator row)
    #     - it is the last word of the row (word_unit.last_of_row is true)
    #     - its x coordinate + width is the global maximum (max(word_unit.x + word_unit.w))
    # Default beginning_row is 3.
    last_of_row_list = filter(lambda t: t.last_of_row and t.row > 2 and t.row < separator_row - 1, word_unit_list)
    extrema_list = [[t.x + t.w , t.row] for t in last_of_row_list if len(t.text) > 0]
    maximum, beginning_row = max(extrema_list, key = operator.itemgetter(0), default = [0, 3])
    # ==================== concluding the x coordinate line =========================
    # Take the x coordinate line to be the smallest x coordinate of the first four product lines.
    x_coordinate_list = filter(lambda t: t.first_of_row and t.row > beginning_row and t.row < beginning_row + 5, word_unit_list)
    extrema_list = [t.x for t in x_coordinate_list if len(t.text) > 0]
    x_coordinate_line = min(extrema_list, default = 45)
    # Loop product rows
    product_main_row = beginning_row
    for i, word_unit in enumerate(word_unit_list):
        # Purchase date
        if word_unit.row == beginning_row and word_unit.last_of_row:
            purchase_date = word_unit.text
            continue
        ################ PRODUCTS #################################
        if word_unit.row > beginning_row and word_unit.row < separator_row and len(word_unit.text) > 0:
            # word_unit is the only one in row - 'PALAUTUS:' or false
            if word_unit.first_of_row and word_unit.last_of_row:
                continue
            # First word of row and word in the x coordinate line - A new product row
            #   - set current row
            #   - append previous product (if there is one)
            #   - init new product unit
            #   - update x coordinate line
            if word_unit.first_of_row and abs(word_unit.x - x_coordinate_line) < 20:
                product_main_row = word_unit.row
                if product_main_row != beginning_row + 1 and 'unit' in locals():
                    product_list.append(unit)
                unit = product.product(purchase_date, '2000', '01', '01', '', '', 0, 0, 1, 0, 0, 0, full_store_name, '')
                x_coordinate_line = word_unit.x
            # Row has not changed - Product row continues
            if word_unit.row == product_main_row:
                # Last word of the row - total price
                if word_unit.last_of_row:
                    try:
                        # Total price 'broken', that is, there is space after the first number. Replace the space with dot.
                        if re.match("^[0-9] ",word_unit.text):
                            word_unit.text = word_unit.text[0] + '.' + word_unit.text[2:]
                        word_unit.text = re.sub("[,]", '.', word_unit.text)
                        unit.total_price = float(word_unit.text)
                    except ValueError:
                        pass
                # Not last word of the row - product name
                else:
                    word_unit.text = replace_scandinavian(word_unit.text)
                    if word_unit.first_of_row:
                        unit.product_name = word_unit.text
                    else:
                        unit.product_name = unit.product_name + ' ' + word_unit.text
            # New row but the first word not in x-coordinate line - Additional information row
            if word_unit.row != product_main_row:
                if word_unit_list[i+1].text == 'KG':
                    try:
                        unit.unit_weight = float(word_unit.text)
                    except ValueError:
                        pass
                elif word_unit_list[i+1].text == 'EUR/KG':
                    try:
                        unit.price_per_weight = float(word_unit.text)
                    except ValueError:
                        pass
                elif word_unit_list[i+1].text == 'KPL':
                    try:
                        unit.units = int(word_unit.text)
                    except ValueError:
                        pass
                elif word_unit_list[i+1].text == 'EUR/KPL':
                    try:
                        unit.unit_price = float(word_unit.text)
                    except ValueError:
                        pass
        # Reached the first separator row - product listing ends
        elif word_unit.row == separator_row and word_unit.first_of_row:
            product_list.append(unit)
        # Check sum
        elif word_unit.row == separator_row + 1 and word_unit.last_of_row:
            check_sum = word_unit.text
    # Return
    return product_list, check_sum

def parse_receipt_L(word_unit_list, separator_row):
    '''
    Parser for Lidl receipts.

    Parameters
    ----------
    word_unit_list
        The list of text parts as a list of word_unit units.
    separator_row
        Row number of separator line.
            
    Returns
    -------
    product_list 
        List of parsed products as a list of product units.
    check_sum
        Check sum of the receipt.
    '''
    product_list = []
    check_sum = 0
    # ======================= concluding the beginning_row ===============================
    # In case of Lidl, the product rows are aligned left. For beginning_row we look for the first left aligned row in the receipt.
    first_of_row_list = filter(lambda t: t.first_of_row and t.x < 99 and t.row > 4 and t.row < separator_row, word_unit_list)
    extrema_list = [t.row for t in first_of_row_list if len(t.text) > 0]
    beginning_row = min(extrema_list, default = 7)
    # Loop product rows 
    product_main_row = beginning_row
    for i, word_unit in enumerate(word_unit_list):
        # Store name is located four rows above the product listing
        if word_unit.row == beginning_row - 4 and word_unit.first_of_row:
            full_store_name = 'Lidl ' + word_unit.text
        # x coordinate line
        if word_unit.row == beginning_row and word_unit.first_of_row:
            x_coordinate_line = word_unit.x
        ################ PRODUCTS #################################
        if word_unit.row >= beginning_row and word_unit.row < separator_row and len(word_unit.text) > 0:
            # First one in the x coordinate line - A new product
            #   - set current row
            #   - append previous product (if there is one)
            #   - init new product unit
            #   - update x coordinate line
            if word_unit.first_of_row and abs(word_unit.x - x_coordinate_line) < 20:
                product_main_row = word_unit.row
                if product_main_row != beginning_row:
                    product_list.append(unit)
                unit = product.product('01012000', '2000', '01', '01', '', '', 0, 0, 1, 0, 0, 0, full_store_name, '')
                x_coordinate_line = word_unit.x
            # Row has not changed - Product row continues
            if word_unit.row == product_main_row:
                # The last word of the row contains a tax class - we pass this
                if word_unit.last_of_row:
                    continue
                # The second word to last - total price
                elif word_unit_list[i+1].last_of_row:
                    try:
                        # Total price 'broken', that is, there is space after the first number. Replace the space with dot.
                        if re.match("^[0-9] ",word_unit.text):
                            word_unit.text = word_unit.text[0] + '.' + word_unit.text[2:]
                        word_unit.text = re.sub("[,]", '.', word_unit.text)
                        unit.total_price = float(word_unit.text)
                    except ValueError:
                        pass
                # Else - product name
                else:
                    word_unit.text = replace_scandinavian(word_unit.text)
                    if word_unit.first_of_row:
                        unit.product_name = word_unit.text
                    else:
                        unit.product_name = unit.product_name + ' ' + word_unit.text
            # Additional information
            if word_unit.row != product_main_row:
                word_unit.text = re.sub("[,]", '.', word_unit.text)
                if word_unit_list[i+1].text == 'kg':
                    try:
                        unit.unit_weight = float(word_unit.text)
                    except ValueError:
                        pass
                elif word_unit_list[i+1].text == 'EUR/kg':
                    try:
                        unit.price_per_weight = float(word_unit.text)
                    except ValueError:
                        pass
                elif word_unit.first_of_row and word_unit_list[i+1].text == 'X':
                    try:
                        unit.units = int(word_unit.text)
                    except ValueError:
                        pass
                elif (word_unit.last_of_row or len(word_unit_list[i+1].text) == 0) and word_unit_list[i-1] == 'X':
                    try:
                        unit.unit_price = float(word_unit.text)
                    except ValueError:
                        pass
        # Reached the first separator row - product listing ends
        elif word_unit.row == separator_row and word_unit.first_of_row:
            product_list.append(unit)
        # Check sum
        elif word_unit.row == separator_row + 1 and word_unit.last_of_row:
            check_sum = word_unit.text
    # return
    return product_list, check_sum
