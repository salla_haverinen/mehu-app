'''
Tools for database.
'''
import operator
import pandas as pd
import pyodbc
import re
import datetime as time

def connection_string(conn_str_or_dict):
    '''
    Return connection string.
    '''
    if isinstance(conn_str_or_dict, dict):
        credential_vars = ['DRIVER','SERVER','DATABASE','UID','PASSWORD','PORT','OPTION']
        conn_str = ';'.join(['{}={}'.format(var, conn_str_or_dict[var]) for var in credential_vars])
    else:
        conn_str = conn_str_or_dict
    # return connection STRING
    return conn_str

class database:
    '''
    Class for database connection.
    '''
    def __init__(self, conn_str_or_dict):
        '''
        Init
        '''
        conn_str = connection_string(conn_str_or_dict)
        # open new connection and set encoding
        connection = pyodbc.connect(conn_str) 
        connection.setdecoding(pyodbc.SQL_WCHAR, encoding='utf-8')
        connection.setencoding(encoding='utf-8')
        # set attributes
        # connection
        # new class dictionary
        # new average info list
        self.connection = connection
        self.product_classes = {}
        self.average_info = []

    def load_product_classes(self):
        '''
        Collects the product key - product class links from the database.
        database.product_classes is a dictionary, where the keys are subsets of product names and the values are product classes.
        '''
        # cursor
        # execute string
        # execute
        # fetch resultset
        cursor = self.connection.cursor()
        exec_string = 'select tuote, luokka from luokitus_link;'
        cursor.execute(exec_string)
        rows = cursor.fetchall()
        # update luokat dictionary
        self.product_classes.update(rows)
        # close the cursor
        cursor.close()

    def set_product_class(self, product):
        '''
        Sets the product class according to product links (database.product_classes).
        The class of the product is set as follows: we loop database.product_classes searching for
        keys which are substrings of product name. In this subset, we pick the longest key. 
        This is because f.ex. in the case of 'valkosipuli', we have matching keys 'sipuli' and 'valkosipuli', 
        which are both right in some sense, but we want to be specific and choose the 'valkosipuli' one.
        '''
        # Proceed only if class info has been collected.
        if len(self.product_classes) <= 0:
            return
        # If class is 'testi' - return
        # Else - proceed
        if product.product_class == 'testi':
            return
        # Initialize resultset
        # Loop product keys. If product key is substring of product name - add key length and class to result list
        class_result = [(len(key), self.product_classes[key]) for key in self.product_classes.keys() if key.lower() in product.product_name.lower()]
        # If result set is not empty
        # Pick the class with greatest length - CASE sipuli & valkosipuli
        # Set class
        if len(class_result) > 0:
            max_len, max_class = max(class_result, key = operator.itemgetter(0))
            product.product_class = max_class
    
    def load_average_info(self):
        '''
        Collects the average weight and price information of the products from the database.
        '''
        # get cursor
        # execute string
        # execute
        # fetch resultset
        cursor = self.connection.cursor()
        exec_string = 'select tuote as product_name, avgpaino as average_weight, avghinta as average_price from tuote_info;'
        cursor.execute(exec_string)
        rows = cursor.fetchall()
        # loop results and add in list
        self.average_info = [{'product_name': row.product_name, 'average_weight': row.average_weight, 'average_price': row.average_price} for row in rows]
        # close the cursor
        cursor.close()
    
    def set_product_weigh(self, product):
        '''
        Sets the product weight according to product average info (database.average_info).
        The product weight is set as follows: we loop database.average_info searching for
        product keys which are substrings of product name. In this subset, we pick ones with the longest product name (remember case valkosipuli). 
        Since some of the products have multiple package sizes, hey have multiple product average info entries. F.ex.
        Oivariini has package sizes 350g and 550g. The packages have different average prices. We choose the package size (weight) which
        average price is closer to product (unit) weight.
        '''
        # Proceed only if average info has been collected
        if len(self.average_info) <= 0:
            return
        # If product has no weight
        # Initialize resultset
        # Loop average rows. If product key is substring of product name - add product name length, price difference and average entry to average_result_list
        if product.unit_weight == 0:
            average_result_list = [{
                'name_length' : len(info['product_name']), 
                'price_diff' : abs(float(product.unit_price) - float(info['average_price'])), 
                'average_info_object' : info 
                } for info in self.average_info if info['product_name'].lower() in product.product_name.lower()] 
            # If the product has price and average_result_list is not empty
            # Find the products with longest product name among average_result_list
            # Find the entry with smallest price difference among products with longest product name
            # Finally, set weight
            if product.unit_price > 0 and len(average_result_list) > 0:
                max_len = max(average_result_list, key = operator.itemgetter('name_length'))
                max_len_average_result_list = [x for x in average_result_list if x['name_length'] == max_len['name_length']]
                average_info_unit = min(max_len_average_result_list, key = operator.itemgetter('price_diff'))['average_info_object']
                product.unit_weight = float(average_info_unit['average_weight'])

    def add_purchase(self, product):
        '''
        Adds the product to the database table 'ostokset'.
        '''
        # cursor
        cursor = self.connection.cursor()
        # add to database
        exec_string = ("{CALL add_ostos (%s, %s, %s, %s, '%s', '%s', %s, %s, %s, %s, %s, %s, '%s', '%s')}" % 
                    (product.date, product.year, product.month, product.day, product.product_class, product.product_name, 
                    product.unit_weight, product.unit_price, product.units, product.total_weight, product.price_per_weight, product.total_price, product.store, product.buyer))
        # execute and commit
        cursor.execute(exec_string)
        cursor.commit()
        cursor.close()
    
    def update_class_links_table(self, product):
        '''
        Updates table luokitus_link.
        Passes if product name is a key in self.product_classes or product class is not set. Else adds product name and product class to table.
        '''
        # collect class information and init count
        self.load_product_classes()
        # cursor
        cursor = self.connection.cursor()
        # if there is product-key for product i.e. product-class link exists, then pass
        if any(key.lower() == product.product_name.lower() for key in self.product_classes.keys()):
            return
        elif product.product_class == '':
            return
        # if not - add to database
        else:
            # call procedure add_luokitus_link, commit and add one to count
            exec_string = ("{CALL add_luokitus_link ('%s', '%s')}" % (product.product_class, product.product_name) )
            cursor.execute(exec_string)
            cursor.commit()
        # close the cursor
        cursor.close()

    def update_average_info_table(self):
        '''
        Generates and inserts the rows of table tuote_info. The rows are generated from the historical puchase data, that is, from table ostokset.
        The rows are generated as follows: First count the number of different weights of product. 
        If the number is less or equal to four, calculate the average unit price inside each weight. For all weights, add product, weight and calculated average unit price to table.
        If the number is greater than four, calculate the average unit weight and average unit price of product. Add product, calculated unit weight and calculated unit price to table.
        '''
        # cursor
        cursor = self.connection.cursor()
        # clear tuote_info with procedure call
        clear_string = "{CALL clear_tuote_info}"
        cursor.execute(clear_string)
        cursor.commit()
        # get the products in table ostokset
        exec_string = "select distinct(tuote) as product_name from ostokset where luokka not in ('testi');"
        cursor.execute(exec_string)
        db_products = cursor.fetchall()
        # init insert string
        insert_string = "INSERT INTO tuote_info (tuote, avgpaino, avghinta) VALUES "
        # loop products in table ostokset
        for db_product in db_products:
            # count of weights
            # execute and fetch
            product_string = ("select count(distinct paino) from ostokset where tuote = '%s' and luokka not in ('testi');" % (db_product.product_name) )
            cursor.execute(product_string)
            distinct_weight_count = cursor.fetchone()
            # CASES
            # if there are 4 or less different weights, then there will be same number of rows in tuote_info table
            if distinct_weight_count[0] <= 4:
                # average info from ostokset
                avg_string = ("select tuote as product_name, paino as average_weight, round(avg(hinta),4) as average_price from ostokset where tuote = '%s' and luokka not in ('testi') group by paino order by paino;" 
                              % (db_product.product_name) )
                # execute, fetch and add to insert string
                cursor.execute(avg_string)
                db_rows = cursor.fetchall()
                for row in db_rows:
                    insert_string += ("('%s', %s, %s)," % (row.product_name, row.average_weight, row.average_price) )
            # if there are more than 4 weights, then there will only be one row in tuote_info
            else:
                # average info from ostokset
                avg_string = ("select tuote as product_name, round(avg(paino),4) as average_weight, round(avg(hinta),4) as average_price from ostokset where tuote = '%s' and luokka not in ('testi');" 
                              % (db_product.product_name) )
                # execute,fetch and add to insert string
                cursor.execute(avg_string)
                db_rows = cursor.fetchall()
                for row in db_rows:
                    insert_string += ("('%s', %s, %s)," % (row.product_name, row.average_weight, row.average_price) )
        # replace the last comma with semicolon
        insert_string = insert_string[:-1] + ';'
        # insert ALL rows, execute and commit
        cursor.execute(insert_string)
        cursor.commit()
        # close the cursor
        cursor.close()
    
    def get_timeline_food_purchases(self, start_date, end_date):
        '''
        Fetches the purchases of given timeline.
        '''     
        # fix date format
        start_day = self.date_handler(start_date)
        end_day = self.date_handler(end_date)
        # if no start or end date - raise erorr
        if start_day is None or end_day is None:
            raise ValueError('Alku tai loppupäivä puuttuu.')
        # if end_day < start_day - raise error
        if end_day < start_day:
            raise ValueError('Loppupäivä ennen alkupäivää. Ei looginen aikajana.')
        # fecth data from given timeline to pandas datafram
        exec_string = "select l.tuoteryhma as product_group, o.luokka as product_class, o.kpl as units, o.yhtpaino as total_weight, o.tothinta as total_price, o.kilohinta as price_per_weight, o.kauppa as store from ostokset o join luokitus l on o.luokka = l.luokka where o.aineistopvm >= '%s' and o.aineistopvm <= '%s' and l.jako='ruoka';" % (start_day, end_day)
        overview_data = pd.read_sql(exec_string, self.connection, columns=['product_group', 'product_class', 'units', 'total_weight', 'total_price', 'price_per_weight', 'store'])
        # return
        return overview_data

    def get_historical_purchases(self):
        '''
        Fetches all historical purchases.
        '''
        # cursor
        cursor = self.connection.cursor()
        # execute str
        exec_string = "select o.vuosi as year, o.kuukausi as month, l.jako as division, l.tuoteryhma as product_group, sum(o.tothinta) as total_sum from ostokset o join luokitus l on o.luokka = l.luokka group by o.vuosi, o.kuukausi, l.jako, l.tuoteryhma;"
        #execute
        cursor.execute(exec_string)
        # fetch resultset
        rows = cursor.fetchall()
        # loop results
        history_data= [[row.year, row.month, row.division, row.product_group, row.total_sum] for row in rows]
        # close the cursor
        cursor.close()
        # sort vuosi, kuukausi, jako
        result_data = sorted(history_data, key=lambda x: (x[0], x[1], x[2]))
        # return
        return result_data

    @staticmethod
    def date_handler(date):
        '''
        Date handler. 
        Transforms the date d.m.yyyy into the form yyyymmdd.
        Returns None if the date is not an actual date or original date not format dd.mm.yyyy.
        '''
        try:
            stripped_date = time.datetime.strptime(date, '%d.%m.%Y')
            formatted_date = stripped_date.strftime('%Y%m%d')
            return formatted_date
        except ValueError:
            return None
        