'''
Tools for handling users.
'''
from . import app

class User:
    '''
    Class for user. Known users and their information is stored in app config.
    '''
    def __init__(self, username, password):
        self.username = username
        self.password = password
        self.id = return_known_id(username, password)

    def is_authenticated(self):
        '''
        Method to authenticate user.
        '''
        # known users
        users = app.config['USERFILE']
        # loop all
        for user in users:
            # if user found in duct list - ok
            if self.username == user['username']:
                if self.password == user['password']:
                    return True
        return False

    def is_active(self):
        return True

    def is_anonymous(self):
        return False

    def get_id(self):
        return self.id

    @classmethod
    def get(cls, id):
        '''
        Get user instance for id.
        '''
        users = app.config['USERFILE']
        for user in users:
            if id == user['id']:
                return cls(user['username'], user['password'])
        return None

def return_known_id(username, password):
    '''
    Returns known user's id. If user is not known returns None.
    '''
    users = app.config['USERFILE']
    for user in users:
        if username == user['username']:
            if password == user['password']:
                return user['id']
    return None