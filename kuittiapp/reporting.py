import os

from urllib.parse import urlparse, urljoin
from flask import jsonify, render_template, request, redirect, url_for, g
from flask_login import LoginManager, login_user, login_required, logout_user
from . import app, reporting_methods, database, user

# Login Manager
login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = 'login'

def is_safe_url(target):
    ref_url = urlparse(request.host_url)
    test_url = urlparse(urljoin(request.host_url, target))
    return test_url.scheme in ('http', 'https') and \
           ref_url.netloc == test_url.netloc

@login_manager.user_loader
def load_user(user_id):
    return user.User.get(user_id)

@app.before_request
def before_request():
    g.db = database.database(app.config['CONNECTION'])

@app.teardown_request
def teardown_request(exception):
    db = getattr(g, 'db', None)
    if db is not None:
        db.connection.close()

@app.route('/login', methods=['GET', 'POST'])
def login():
    error = None
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']
        in_user = user.User(username, password)
        # auth
        if in_user.is_authenticated():
            login_user(in_user)
            next = request.args.get('next')
            # check if the url is safe for redirects.
            if not is_safe_url(next):
                return flask.abort(400)
            return redirect(next or url_for('ruoka'))
    return render_template('login.html', error=error)

@app.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect(url_for('login'))

@app.route("/ruoka", methods=['GET'])
@login_required
def ruoka():
    return render_template('ruoka.html')

@app.route("/get_ruoka", methods=['GET'])
@login_required
def get_ruoka():
    # request dates and get data according to dates
    start_date = request.args.get('start_date')
    end_date = request.args.get('end_date')
    try:
        overview_sum, overview_pie, group_view_list = reporting_methods.ruoka_get_data(start_date, end_date, g.db)
        error = ''
    except Exception as err:
        overview_sum, overview_pie, group_view_list = (0, [], [])
        error = err.args
    # return
    return jsonify({ 'total_sum' : overview_sum, 'portions' : overview_pie, 'products' :  group_view_list, 'error' : error})

@app.route("/kulutus", methods=['GET'])
@login_required
def kulutus():
    history_data = reporting_methods.kulut_get_data(g.db)
    return render_template('kulutus.html', data = history_data)

@app.route("/kuitti", methods=['GET', 'POST'])
@login_required
def kuitti():
    if request.method == 'POST':
        # request image
        file = request.files['image']
        # save uploaded file
        uploaded_file = os.path.join(app.config['UPLOAD_FOLDER'], file.filename)
        file.save(uploaded_file)
        # read kuitti
        try:
            product_dict_list, check_sum = reporting_methods.receipt_reader(uploaded_file, g.db, app.config)
            error = ''
        except Exception as err:
            product_dict_list = []
            check_sum = []
            error = err.args
        # return
        return jsonify({'products' : product_dict_list, 'check_sum' : check_sum, 'error' : error})

    return render_template('kuitti.html')

@app.route("/kuitti_update", methods=['POST'])
@login_required
def kuitti_update():
    data = request.get_json(force=True)
    product_dict_list, check_sum =reporting_methods.update_receipt(data, g.db)
    return jsonify({'products' : product_dict_list, 'check_sum' : check_sum})

@app.route("/kuitti_send", methods=['POST'])
@login_required
def kuitti_send():
    data = request.get_json(force=True)
    prods_count = reporting_methods.send_receipt(data, g.db, app.config)
    return jsonify({'products' : prods_count})

@app.route("/database_update", methods=['GET'])
@login_required
def base_update():
    reporting_methods.update_average_info_table(g.db)
    return redirect(url_for('features', message = 'Tuoteinfo päivitetty'))

@app.route("/database_backup", methods=['GET'])
@login_required
def database_backup():
    try:
        reporting_methods.database_backup(app.config)
        message = 'database varmuuskopioitu'
    except:
        message = 'Tietokannan varmuuskopiointi epäonnistui'
    return redirect(url_for('features', message = message))

@app.route("/features", methods=['GET'])
@login_required
def features():
    return render_template('features.html')

@app.route("/ostos", methods=['GET','POST'])
@login_required
def ostos():
    if request.method == 'POST':
        form = request.form
        reporting_methods.save_purchase(form, g.db)
    return render_template('ostos.html')

@app.route("/signin-microsoft", methods=['GET'])
def get_code():
    return 'YADDA'