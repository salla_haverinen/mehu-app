from flask import Flask
import argparse

# optional environment argument - default is dev
parser = argparse.ArgumentParser(description='Sallan Flask app')
parser.add_argument('-env', choices=['dev', 'test', 'prod'], default='dev', 
    help='Environment variable. OBS, prod and dev use different databases.', )
# parse only known args
env = vars(parser.parse_known_args()[0]).get("env")
# init app
app = Flask(__name__)

# configuration according to environment
if env == 'dev':
    app.config.from_object('config.DevelopmentConfig')
elif env == 'test':
    app.config.from_object('config.TestConfig')
elif env == 'prod':
    app.config.from_object('config.ProductionConfig')
else:
    raise ValueError('Invalid environment name')

from kuittiapp import reporting

