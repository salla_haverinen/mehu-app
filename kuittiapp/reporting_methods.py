import numpy as np
import pandas as pd

from .lib import backup, parser, product, reader, wordbook

def receipt_reader(file_path, connection, app_config):
    '''
    Receipt reader. Calls the methods to scan and read the receipt.
    Raises error if
        - no receipt is recognized from the original picture
        - no store is identified from the receipt

    Returns the read receipt as list of dictionaries and check sum of the receipt.

    Parameters
    ----------
    file_path
        Path to file.
    connection
        Database connection class instance.
    app_config
        Application configs.

    Returns
    -------
    product_dict_list
        List of read, parsed and updated products (list of dictionaries).
    check_sum
        Check sum of the recepit.
    '''
    scanned_image = reader.scan_image(file_path)
    RATIO = 1575/np.size(scanned_image, 1)
    modified_image = reader.modify_image(scanned_image, ratio= RATIO, gray = 1, blur_filter = 1, blur_gaus = 0, treshold = 1, inverse = 0)
    word_unit_list = reader.detect_words(modified_image, 'Unknown')
    store_name = reader.recognize_store(app_config['TESSERACT'], modified_image, word_unit_list)
    word_unit_list = reader.detect_words(modified_image, store_name)
    separator_row = reader.find_separator_row(word_unit_list)
    reader.threaded_read_with_tesseract(app_config['TESSERACT'], modified_image, separator_row, word_unit_list)
    product_list, check_sum = parser.parse_receipt(word_unit_list, separator_row, store_name)
    wordbook.write_product_log(product_list, app_config['LOKIFILE'], state=0)
    connection.load_average_info()
    wordbook.fix_product_names(product_list, connection.average_info, app_config['WORDLIST'])
    product_dict_list = list(map(lambda unit: unit.product_to_dict(), product_list))
    return product_dict_list, check_sum

def update_receipt(product_dict_list, connection):
    '''
    Updates the values of products.

    Parameters
    ----------
    product_dict_list
        List of products (list of dictionaries).
    connection
        Database connection class instance.

    Returns
    -------
    result_list
        Updated list of products (list of dictionaries).
    c_sum
        Check sum of receipt.
    '''
    # get product classes from database
    connection.load_product_classes()
    # get product info from database
    connection.load_average_info()
    # update products
    product_list = [product.product.dict_to_product(product_dict) for product_dict in product_dict_list]
    for unit in product_list:
        unit.update_unit_price()
        connection.set_product_class(unit)
        connection.set_product_weigh(unit)
        unit.count_price_per_weight()
        unit.count_total_weight()
        unit.update_purchase_date()
    c_sum = sum([unit.total_price for unit in product_list])
    result_list = [unit.product_to_dict() for unit in product_list]
    return result_list, c_sum

def send_receipt(product_dict_list, connection, app_config):
    '''
    Writes the products to database table. Adds new links to database and writes new words to Tesseract word file.

    Parameters
    ----------
    product_dict_list
        List of products (list of dictionaries).
    connection
        Database connection class instance.
    app_config
        Application configs.

    Returns
    -------
    added_prods
        Number of added products.
    '''
    product_list = [product.product.dict_to_product(product_dict) for product_dict in product_dict_list]
    for unit in product_list:
        connection.add_purchase(unit)
        connection.update_class_links_table(unit)
    wordbook.update_word_list(product_list, app_config['WORDLIST'])
    wordbook.write_product_log(product_list, app_config['LOKIFILE'], state=1)
    return len(product_list)

def save_purchase(form, connection):
    '''
    Save manually typed purchase to database.

    Parameters
    ----------
    form
        Form containing product information.
    connection
        Database connection class instance.
    '''
    product_dict = {
        'date' : form['purchase-date'],
        'year' : 0,
        'month' : 0,
        'day' : 0,
        'product_class' : form['purchase-class'],
        'product_name' : form['purchase-name'],
        'unit_weight' : 0,
        'unit_price' : 0,
        'units' : 1,
        'total_weight': 0,
        'total_price' : form['purchase-price'],
        'price_per_weight' : 0,
        'store' : form['store'],
        'buyer' : form['buyer']
    }
    unit = product.product.dict_to_product(product_dict)
    unit.update_unit_price()
    unit.update_purchase_date()
    connection.add_purchase(unit)

def wavg(group):
    '''
    Weighted average. Counts the weighted average of price per weight.

    Parameters
    ----------
    group
        Product group
    
    Returns
    -------
    wavg
        Weighted average of price per weight.
    '''
    d = group['price_per_weight']
    w = group['units']
    wavg = (d * w).sum() / w.sum()
    return wavg

def ruoka_get_data(start_d, end_d, connection):
    '''
    Ruoka page data fetcher. Fetches the data from the database between start date and end date.
    Raises error if
        - one or both dates are missing
        - end date is before start date
        - there is no data

    Parameters
    ----------
    start_d
        Start date.
    end_d
        End date.
    connection
        Database connection class instance.

    Returns
    -------
    overview_sum
        Overall spent money of the given time period.
    overview_pie
        Sum of spend money and portion of the overall spend money grouped by product group.
    group_view_list
        Sum of spend money, sum of weight and weighted average of kilohinta group by products.
    error
        Possible error message.
    '''
    overview_data = connection.get_timeline_food_purchases(start_d, end_d)
    # calc overview sum
    overview_sum = float(overview_data['total_price'].sum())
    # calc overview portions
    overview_osuudet = overview_data.groupby(['product_group'])['total_price'].agg('sum').reset_index()
    overview_pie = overview_osuudet[['product_group', 'total_price']]
    overview_pie['total_price'] = overview_pie['total_price'].astype(float)
    overview_pie['portion'] = overview_pie['total_price']/overview_sum
    # calc sum of price, weight and weighted average of price per unit of product classes
    overview_group = overview_data.groupby(['product_group', 'product_class']).agg({'total_price': 'sum', 'total_weight' : 'sum'}).reset_index()
    price_per_weight_list = overview_data.groupby(['product_group', 'product_class']).apply(wavg).tolist()
    group_view_list = overview_group[['product_group', 'product_class', 'total_price', 'total_weight']]
    group_view_list['total_price'] = group_view_list['total_price'].astype(float)
    group_view_list['total_weight'] = group_view_list['total_weight'].astype(float)
    group_view_list['price_per_weight'] = [float(price_per_weight) for price_per_weight in price_per_weight_list]
    # return
    return overview_sum, overview_pie.to_dict('records'), group_view_list.to_dict('records')

def kulut_get_data(connection):
    '''
    Kulut page data fetcher.
    This method returns the history data fetched from database in a list of dictionaries with the following hierarchy:
    [{
        year : <year number>,
        sum : <total purchases of year>
        month : [{
                    mm : <month number>,
                    sum : <total purchases of month>,
                    data : [{
                                division : <division name [ruoka|kulutus]>,
                                sum : <total purchases of division>,
                                data : [{
                                            product_group : <name of the product group>,
                                            sum : <total purchases of product group>
                                        },
                                        {
                                            product_group : <name of the product group>,
                                            sum : <total purchases of product group>
                                        }]
                             }]
                }]
    },
    {
        year : <year number>,
        sum : <total purchases of year>
        month : [...]
    }]
    
    Output from database (history_data) is a list of lists. Inner lists have 5 elements in them:
    0 = year, 1 = month, 2 = division, 3 = product_group, 4 = total_sum.

    Parameters
    ----------
    connection
        Database connection class instance.
    
    Returns
    -------
    history_data_dict_list
        History data list of dictionaries.
    '''
    history_data = connection.get_historical_purchases()
    # check data - if not enought rows - return empty list
    if len(history_data) < 2:
        return []
    # else init and loop
    history_data_dict_list = []
    for i, history_element in enumerate(history_data):
        # NEW DATA
        new_unit = {}
        new_unit['product_group'] = history_element[3]
        new_unit['sum'] = float(history_element[4])

        # NEW YEAR
        if i == 0 or history_data[i-1][0] != history_element[0]:
            # commit previous
            if i != 0:
                res_dict['month'] = month_parents
                history_data_dict_list.append(res_dict)
            
            # init children
            month_parents = []
            month_children = []
            divison_children = []
            # init new dict
            res_dict = {}
            res_dict['year'] = history_element[0] 
            res_dict['sum'] = float(sum([entry[4] for entry in history_data if entry[0] == history_element[0]]))
            res_dict['month'] = month_parents

        # only new month
        if i == 0 or history_data[i-1][0] != history_element[0] or history_data[i-1][1] != history_element[1]:
            month_children = []
            divison_children = []
            new_month = {}
            new_month['mm'] = history_element[1]
            new_month['sum'] = float(sum([entry[4] for entry in history_data if entry[0] == history_element[0] and entry[1] == history_element[1]]))
            new_month['data'] = month_children
            month_parents.append(new_month)

        # only new partition
        if i == 0 or history_data[i-1][0] != history_element[0] or history_data[i-1][1] != history_element[1] or history_data[i-1][2] != history_element[2]:
            divison_children = []
            new_division = {}
            new_division['division'] = history_element[2]
            new_division['sum'] = float(sum([entry[4] for entry in history_data if entry[0] == history_element[0] and entry[1] == history_element[1] and entry[2] == history_element[2]]))
            new_division['data'] = divison_children
            month_children.append(new_division)
            divison_children.append(new_unit)
        else:
            divison_children.append(new_unit)
        
        # last commit
        if i == len(history_data) - 1:
            history_data_dict_list.append(res_dict)
    # return
    return history_data_dict_list

def update_average_info_table(connection): 
    '''
    Updates product info table in database.

    Parameters
    ----------
    connection
        Database connection class instance.
    '''
    connection.update_average_info_table()

def database_backup(app_config):
    '''
    Creates backup of the database and send it, with other log files, to OneDrive.

    Parameters
    ----------
    app_config
        Application configs.
    '''
    OD_BACKUP_FOLDER = app_config['OD_BACKUP_FOLDER']
    backups = []
    # database backup
    db_backup_path = backup.create_database_backup(app_config)
    backups.append((db_backup_path, OD_BACKUP_FOLDER))
    # product name backup
    backups.append((app_config['LOKIFILE'], OD_BACKUP_FOLDER)) 
    try:
        backup.backup_to_drive(app_config, backups)
    except Exception as err:
        print('[error] {}'.format(err.args))
        raise
