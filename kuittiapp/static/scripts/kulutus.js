// ==== DEVICE DETECTION ====

// init as false
var isMobile = false;
// true if mobile
if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i
    .test(navigator.userAgent)
    || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i
        .test(navigator.userAgent.substr(0, 4))) {
    isMobile = true;
}

/* Map names of the months */
let month = d3.scale.ordinal()
    .range(['Tammikuu', 'Helmikuu', 'Maaliskuu', 'Huhtikuu', 'Toukokuu', 'Kesäkuu',
        'Heinäkuu', 'Elokuu', 'Syyskuu', 'Lokakuu', 'Marraskuu', 'Joulukuu'])
    .domain([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]);

/* Function to format numbers (euro) */ 
function formatSum (sum) {
    return sum.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
}

/* Format data */
function displayHistory(historyData) {
    // Base div
    mainDiv = d3.select('.overview').append("div").attr('id', 'view-history');

    /* Sort data*/
    historyData.forEach(function (d) {
        // format sum
        d.sum = formatSum(d.sum);
        // sort months
        d.month = d.month.sort(function (a, b) {
            let mm1 = Number(a.mm)
            let mm2 = Number(b.mm)
            if (mm1 > mm2) return 1;
            if (mm1 < mm2) return -1;
            return 0;
        });
        d.month.forEach(function (d) {
            // format sum
            d.sum = formatSum(d.sum);
            // sort data divisions
            d.data = d.data.sort(function (a, b) {
                if (a.division > b.division) return 1;
                if (a.division < b.division) return -1;
                return 0;
            });
            d.data.forEach(function (d) {
                // format sum
                d.sum = formatSum(d.sum);
                // sort product groups
                d.data = d.data.sort(function (a, b) {
                    if (a.product_group > b.product_group) return 1;
                    if (a.product_group < b.product_group) return -1;
                    return 0;
                });
            });
        });
    });

    /* Create tree */
    mainDiv.selectAll("div").data(historyData)
        .enter().append('div').attr('class', 'div-year')
        .each(function () {
            let yearLevel = d3.select(this);

            yearButton = yearLevel.append('button').attr('class', 'level-year').on('click', levelClick);
            yearButton.append('p').attr('class', 'label-year').text(function (d) { return d.year; })
            yearButton.append('p').attr('class', 'sum-year').text(function (d) { return d.sum + 'e'; })

            yearLevel.append('div').attr('class', 'div-months').selectAll('div').data(function (d) { return d.month; })
                .enter().append('div').attr('class', 'div-month')
                .each(function () {
                    let monthLevel = d3.select(this);

                    monthButton = monthLevel.append('button').attr('class', 'level-month').on('click', levelClick);
                    monthButton.append('p').attr('class', 'label-month').text(function (d) { return month(d.mm); })
                    monthButton.append('p').attr('class', 'sum-month').text(function (d) { return d.sum + 'e'; })

                    monthLevel.append('div').attr('class', 'div-divisions').selectAll('div').data(function (d) { return d.data; })
                        .enter().append('div').attr('class', 'div-division')
                        .each(function () {
                            let divisionLevel = d3.select(this);

                            divisionButton = divisionLevel.append('button').attr('class', 'level-division').on('click', levelClick);
                            divisionButton.append('p').attr('class', 'label-division').text(function (d) { return d.division; });
                            divisionButton.append('p').attr('class', 'sum-divison').text(function (d) { return d.sum + 'e'; });

                            divisionLevel.append('div').attr('class', 'div-groups').selectAll('div').data(function (d) { return d.data; })
                                .enter().append('p').attr('class', 'level-group').text(function (d) { return d.product_group + ' ' + d.sum + 'e'; })
                        })
                })

        })

    /* Click funtionality */
    function levelClick(d) {
        this.classList.toggle("active");
        var nextDiv = this.nextElementSibling;
        if (nextDiv.style.maxHeight) {
            nextDiv.style.maxHeight = null;
        } else {
            //nextDiv.style.maxHeight = nextDiv.scrollHeight + "px";
            nextDiv.style.maxHeight = '1000px'
        }
    }
}
