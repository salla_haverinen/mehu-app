// ==== DEVICE DETECTION ====

// init as false
var isMobile = false;
// true if mobile
if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i
    .test(navigator.userAgent)
    || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i
        .test(navigator.userAgent.substr(0, 4))) {
    isMobile = true;
}

// First build product table
// Header and table for product list
let receiptFrame = d3.select('#receipt-result').append("div").attr('id', 'receipt-frame');
let receiptTable = d3.select('#receipt-frame').append("table").attr('id', 'receipt-list');
let receiptHead = receiptTable.append("thead").attr("class", "receipt-list-head");
let receiptBody = receiptTable.append("tbody");
let receiptTotal = receiptTable.append("tfoot");

// Append the table header row
receiptHead.append('tr')
    .selectAll('th')
    .data(['Pvm', 'Luokka', 'Tuote', 'Paino',
        'Hinta', 'Kpl', 'Yht paino', 'Yht hinta', 'Kilohinta', 'Kauppa', '  ']).enter()
    .append('th')
    .text(function (d) { return d; });

// Append table total row
let receiptTot = receiptTotal.append('tr');
receiptTot.append('th')
    .attr('colspan', '6');
receiptTot.append('th')
    .text('Total :')
    .attr('id', 'totalCell')
    .attr('colspan', '1');
receiptTot.append('td')
    .attr('id', 'sumCell')
    .style('font-weight', 'bold')
    .style('text-align', 'right');

// Append buttons
d3.select('#receipt-result').append("div").attr('id', 'receipt-buttons');
d3.select('#receipt-buttons')
    .append("button")
    .attr('id', 'addbutton')
    .text('Lisää rivi')
    .on('click', addTableRow);

d3.select('#receipt-buttons')
    .append("button")
    .attr('id', 'updatebutton')
    .text('Päivitä')
    .on('click', updateData);

d3.select('#receipt-buttons')
    .append("button")
    .attr('id', 'finishbutton')
    .text('Tallenna')
    .on('click', sendDatabase);

// Function to preview the image
function previewFile() {
    let preview = document.querySelector('img');
    let file = document.querySelector('input[type=file]').files[0];
    let reader = new FileReader();

    reader.onloadend = function () {
        preview.src = reader.result;
        preview.width = isMobile ? 0.8 * window.screen.width : 350;
        // Hide possible previous kuitti
        document.getElementById('receipt-result').style.display = "none";

    }
    if (file) {
        reader.readAsDataURL(file);
    } else {
        preview.src = "";
    }
}

// Submit action (image upload)
$('#receipt-upload').submit(function (event) {
    // prevent default
    event.preventDefault();
    // image to formData object
    let up_image = new FormData($('#receipt-upload')[0]);
    // if there is image...
    // Show loading animation and pass image to server with ajax
    // After response from server, hide animation
    // If errors occur, display them to user
    // Else, display product data
    if (up_image.get('image').name) {
        loadAnimation();
        $.ajax({
            type: 'POST',
            url: '/kuitti',
            data: up_image,
            contentType: false,
            cache: false,
            processData: false,
            success: function (data) {
                if (data.error.length > 0) {
                    hideAnimation();
                    alert(data.error);
                }
                else {
                    updateProductList(data.products, 0);
                }
            }
        });
    }
    else {
        alert('Valitse ensin ladattava kuitti');
    }
});

// Function to display loading animation
function loadAnimation() {
    // blur and show load circle
    $('div').not('.loader').not('.loader-content').css('filter', 'blur(2px');
    document.getElementById("loader").style.display = "block";
}

// Function to hide loading animation
function hideAnimation() {
    // remove blur and hide load circle
    $('div').not('.loader').not('.loader-content').css('filter', 'none');
    document.getElementById("loader").style.display = "none";
}

// Function to update product list 
function updateProductList(incomingData, incomingSum) {
    // hide animation and set receipt-result visible
    hideAnimation();
    document.getElementById('receipt-result').style.display = "block";
    var newData = incomingData.map(function (item) {
        return {
            date: item.date,
            year: item.year,
            month: item.month,
            day: item.day,
            product_class: item.product_class,
            product_name: item.product_name,
            unit_weight: item.unit_weight,
            unit_price: item.unit_price,
            units: item.units,
            total_weight: item.total_weight,
            total_price: item.total_price,
            price_per_weight: item.price_per_weight,
            buyer: item.buyer,
            store: item.store
        };
    });

    // ENTER DATA ROWS
    let rows = receiptBody.selectAll("tr").data(newData);
    let rowsEnter = rows.enter().append('tr');
    // Date column
    rowsEnter.append("td")
        .append("input")
        .attr("type", "text")
        .attr("name", "date")
        .attr("class", "kDateCol")
        .on("change", function (d) {
            d3.selectAll(".kDateCol")
                .property("value", $(this).prop('value'))
                .each(function (d) {
                    d.date = $(this).prop('value');
                });
        });
    // Product class column 
    rowsEnter.append("td")
        .append("input")
        .attr("type", "text")
        .attr("name", "product_class")
        .attr("class", "kProductClassCol")
        .on("change", function (d) {
            d.product_class = $(this).prop('value');
            console.log($(this).prop('value'))
            console.log(d.product_class);
        });
    // Product name column 
    rowsEnter.append("td")
        .append("input")
        .attr("type", "text")
        .attr("name", "product_name")
        .attr("class", "kProductNameCol")
        .on("change", function (d) {
            d.product_name = $(this).prop('value');
        });
    // Unit weight column 
    rowsEnter.append("td")
        .append("input")
        .attr("type", "text")
        .attr("name", "unit_weight")
        .attr("class", "kUnitWeightCol")
        .on("change", function (d) {
            d.unit_weight = $(this).prop('value');
        });
    // Unit price column 
    rowsEnter.append("td")
        .append("input")
        .attr("type", "text")
        .attr("name", "unit_price")
        .attr("class", "kUnitPriceCol")
        .on("change", function (d) {
            d.unit_price = $(this).prop('value');
        });
    // Units column 
    rowsEnter.append("td")
        .append("input")
        .attr("type", "text")
        .attr("name", "units")
        .attr("class", "kUnitsCol")
        .on("change", function (d) {
            d.units = $(this).prop('value');
        });
    // Total weight column 
    rowsEnter.append("td")
        .append("input")
        .attr("type", "text")
        .attr("name", "total_weight")
        .attr("class", "kTotalWeightCol")
        .on("change", function (d) {
            d.total_weight = $(this).prop('value');
        });
    // Total price column 
    rowsEnter.append("td")
        .append("input")
        .attr("type", "text")
        .attr("name", "total_price")
        .attr("class", "kTotalPriceCol")
        .on("change", function (d) {
            d.total_price = $(this).prop('value');
        });
    // Price per weight column 
    rowsEnter.append("td")
        .append("input")
        .attr("type", "text")
        .attr("name", "price_per_weight")
        .attr("class", "kPricePerWeightCol")
        .on("change", function (d) {
            d.price_per_weight = $(this).prop('value');
        });
    // store column 
    rowsEnter.append("td")
        .append("input")
        .attr("type", "text")
        .attr("name", "store")
        .attr("class", "kStoreCol")
        .on("change", function (d) {
            d3.selectAll(".kStoreCol")
                .property("value", $(this).prop('value'))
                .each(function (d) {
                    d.store = $(this).prop('value');
                });
        });
    // Delete button
    rowsEnter.append("td")
        .append("input")
        .attr("type", "button")
        .attr("name", "del")
        .attr("value", "X")
        .attr("class", "deleteCol")
        .on("click", function () {
            $(this).closest('tr').remove()

        });

    // Update column values
    d3.selectAll(".kDateCol").data(newData).property("value", function (d) {
        return d.date;
    });
    d3.selectAll(".kProductClassCol").data(newData).property("value", function (d) {
        return d.product_class;
    });
    d3.selectAll(".kProductNameCol").data(newData).property("value", function (d) {
        return d.product_name;
    });
    d3.selectAll(".kUnitWeightCol").data(newData).property("value", function (d) {
        return d.unit_weight.toFixed(3);
    });
    d3.selectAll(".kUnitPriceCol").data(newData).property("value", function (d) {
        return d.unit_price.toFixed(2);
    });
    d3.selectAll(".kUnitsCol").data(newData).property("value", function (d) {
        return d.units;
    });
    d3.selectAll(".kTotalWeightCol").data(newData).property("value", function (d) {
        return d.total_weight.toFixed(3);
    });
    d3.selectAll(".kTotalPriceCol").data(newData).property("value", function (d) {
        return d.total_price.toFixed(2);
    });
    d3.selectAll(".kPricePerWeightCol").data(newData).property("value", function (d) {
        return d.price_per_weight.toFixed(3);
    });
    d3.selectAll(".kStoreCol").data(newData).property("value", function (d) {
        return d.store;
    });

    // Remove
    rows.exit().remove();

    // If incomingSum is 0 - count the sum from data and insert to #sumCell
    // Else update the #sumCell with incomingSum
    if (incomingSum == 0) {
        var sum = d3.sum(incomingData, function (d) { return d.total_price; });
        d3.select('#sumCell')
            .text(sum.toFixed(2));
    }
    else {
        d3.select('#sumCell')
            .text(incomingSum.toFixed(2));
    }
}

// Function to get the last child of selection
d3.selection.prototype.last = function () {
    let last = this.size() - 1;
    return d3.select(this[0][last]);
};

// Function to add row to table
function addTableRow() {
    // Get reference to current last row of table, add new row to the end, select the new last row and append columns to row
    lastRow = receiptBody.selectAll("tr").last();
    let table = document.getElementById('receipt-list').getElementsByTagName('tbody')[0];
    table.insertRow(table.rows.length);
    addedRow = receiptBody.selectAll("tr").last();
    addedRow.property("__data__", {
        date: lastRow.property("__data__").date,
        year: lastRow.property("__data__").year,
        month: lastRow.property("__data__").month,
        day: lastRow.property("__data__").day,
        product_class: "",
        product_name: "Uusi tuote",
        unit_weight: "0",
        unit_price: "0",
        units: "1",
        total_weight: "0",
        total_price: "0",
        price_per_weight: "0",
        buyer: lastRow.property("__data__").buyer,
        store: lastRow.property("__data__").store
    });
    // Date column
    addedRow.append("td")
        .append("input")
        .attr("type", "text")
        .attr("name", "date")
        .attr("class", "kDateCol")
        .on("change", function (d) {
            d3.selectAll(".kDateCol")
                .property("value", $(this).prop('value'))
                .each(function (d) {
                    d.date = $(this).prop('value')
                });
        });
    // Product class column 
    addedRow.append("td")
        .append("input")
        .attr("type", "text")
        .attr("name", "product_class")
        .attr("class", "kProductClassCol")
        .on("change", function (d) {
            d.product_class = $(this).prop('value');
        });;
    // Product name column 
    addedRow.append("td")
        .append("input")
        .attr("type", "text")
        .attr("name", "product_name")
        .attr("class", "kProductNameCol")
        .on("change", function (d) {
            d.product_name = $(this).prop('value');
        });;
    // Init weight column 
    addedRow.append("td")
        .append("input")
        .attr("type", "text")
        .attr("name", "unit_weight")
        .attr("class", "kUnitWeightCol")
        .on("change", function (d) {
            d.unit_weight = $(this).prop('value');
        });;
    // Unit price column 
    addedRow.append("td")
        .append("input")
        .attr("type", "text")
        .attr("name", "unit_price")
        .attr("class", "kUnitPriceCol")
        .on("change", function (d) {
            d.unit_price = $(this).prop('value');
        });
    // Units column 
    addedRow.append("td")
        .append("input")
        .attr("type", "text")
        .attr("name", "units")
        .attr("class", "kUnitsCol")
        .on("change", function (d) {
            d.units = $(this).prop('value');
        });
    // Total Weight column 
    addedRow.append("td")
        .append("input")
        .attr("type", "text")
        .attr("name", "total_weight")
        .attr("class", "kTotalWeightCol")
        .on("change", function (d) {
            d.total_weight = $(this).prop('value');
        });;
    // Total price column 
    addedRow.append("td")
        .append("input")
        .attr("type", "text")
        .attr("name", "total_price")
        .attr("class", "kTotalPriceCol")
        .on("change", function (d) {
            d.total_price = $(this).prop('value');
        });;
    // Price per weight column 
    addedRow.append("td")
        .append("input")
        .attr("type", "text")
        .attr("name", "price_per_weight")
        .attr("class", "kPricePerWeightCol")
        .on("change", function (d) {
            d.price_per_weight = $(this).prop('value');
        });;
    // Store column 
    addedRow.append("td")
        .append("input")
        .attr("type", "text")
        .attr("name", "store")
        .attr("class", "kStoreCol")
        .on("change", function (d) {
            d3.selectAll(".kStoreCol")
                .property("value", $(this).prop('value'))
                .each(function (d) {
                    d.store = $(this).prop('value');
                });
        });
    // Delete button
    addedRow.append("td")
        .append("input")
        .attr("type", "button")
        .attr("name", "del")
        .attr("value", "X")
        .attr("class", "deleteCol")
        .on("click", function () {
            $(this).closest('tr').remove()
        });
}

// Function for update button action
function updateData() {
    // When update button is clicked
    // Init data list, append all rows to list and send data list to server
    let riviList = [];
    receiptBody.selectAll('tr').each(function (d) {
        riviList.push(d)
    });
    $.ajax({
        type: 'POST',
        url: "/kuitti_update",
        data: JSON.stringify(riviList),
        contentType: "application/json",
        dataType: "json",
        success: function (data) {
            updateProductList(data.products, data.check_sum);
        }
    });
}

// Function for send button
function sendDatabase() {
    // When update button is clicked
    // Init data list, append all rows to list and send data list to server
    let riviList = [];
    receiptBody.selectAll('tr').each(function (d) {
        riviList.push(d)
    });
    $.ajax({
        type: 'POST',
        url: "/kuitti_send",
        data: JSON.stringify(riviList),
        contentType: "application/json",
        dataType: "json",
        success: function (data) {
            afterSend(data.products);
        }
    });
}

// Function for after send action
function afterSend(prods) {
    // Blur and display popup
    $('div').not('.popup').not('.popup-content').css('filter', 'blur(2px');
    let pop = document.getElementById('popup');
    let outputText = document.getElementById('popup-print');
    outputText.innerHTML = "Tietokantaan lisätty " + prods + " tuotetta.";
    pop.style.display = "block";
    let closeB = document.getElementsByClassName("popup-close")[0];
    // When the user clicks (x), close the popup and redirect
    closeB.onclick = function () {
        closePopup()
    }
    // When the user clicks anywhere outside of the popup, close it and redirect
    window.onclick = function (event) {
        if (event.target == pop) {
            closePopup()
        }
    }
    // Closing function
    function closePopup() {
        // Remove blur, set display and reload page
        $('div').not('.popup').not('.popup-content').css('filter', 'none');
        pop.style.display = "none";
        window.location.href = window.location.href;
    }

}
