// ==== DEVICE DETECTION ====

// init as false
var isMobile = false;
// true if mobile
if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i
    .test(navigator.userAgent)
    || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i
        .test(navigator.userAgent.substr(0, 4))) {
    isMobile = true;
}

// ==== INIT START ====
// Set default timeline to last month and call function overview()
let date = new Date();
let firstDay = new Date(date.getFullYear(), date.getMonth() - 1, 1);
let lastDay = new Date(date.getFullYear(), date.getMonth(), 0);
let $startDate = $('#start-date');
$startDate.datepicker({ dateFormat: 'd.m.yy' });
$startDate.datepicker('setDate', firstDay.toLocaleDateString('fi-FI'));
let $endDate = $('#end-date');
$endDate.datepicker({ dateFormat: 'd.m.yy' });
$endDate.datepicker('setDate', lastDay.toLocaleDateString('fi-FI'));
overview();

// Function for overview
function overview() {

    // colors
    var col1 = "#E99A3E", // Eines
        col2 = "#A2D93A", // Kasvikset ja vihannekset
        col3 = "#BD337E", // Kahvi ja tee
        col4 = "#BD337E", // Mehukeitot ja muut juomat 
        col5 = "#8EB7CB", // Lihatuotteet
        col6 = "#8EB946", // Mausteet
        col7 = "#4D88A4", // Maitotuotteet
        col8 = "#D05C9B", // Muu
        col9 = "#A2D93A", // Siemenet ja pahkinat
        col10 = "#FFBD71" // Viljatuotteet

    // overview div id
    let overId = '#view-groups'
    // product div id
    let prodID = '#view-products'

    // init groupList and selectedProd
    var groupList = [];
    var selectedProd = null;

    // time format
    var timeFormat = d3.time.format("%d.%m.%Y");

    // ============== OVERVIEW =======================
    // pie dim
    let width = isMobile ? 0.8 * window.screen.width : 0.2 * window.screen.width,
        height = width,
        radius = width / 2; //Math.min(width, height) / 2;

    // label adjustments
    let xAdd = 10, yAdd = 2;

    // headers and table for top list
    d3.select(overId).append("div").attr('id', 'top-frame');
    d3.select('#top-frame').append("h2").attr('id', 'top-title');
    d3.select('#top-frame').append("p").attr('id', 'overview-title');
    let topTable = d3.select('#top-frame').append("table").attr('class', 'top-list');
    let topHead = topTable.append("thead");
    let topBody = topTable.append("tbody");

    // append headers for top list
    d3.selectAll("#overview-title").text('Ajanjakson ruokakulut \u00A0 ').append("span").attr('id', 'overview-cost');
    d3.selectAll("#top-title").text('Yhteenveto');

    // append the table header row for top list
    topHead.append('tr')
        .selectAll('th')
        .data(['Tuoteryhmä', 'Osuus (%)', 'Hinta (eur)']).enter()
        .append('th')
        .text(function (d) { return d; });

    // div for svg
    d3.select(overId).append("div").attr("id", "pie-frame");

    // svg for pie chart
    let piesvg = d3.select("#pie-frame").append("svg")
        .attr("width", width).attr("height", height).append("g");

    // classes for slices and labels
    piesvg.append("g").attr("class", "slices");
    piesvg.append("g").attr("class", "labels");

    // arc function
    let arc = d3.svg.arc().innerRadius(radius * 0.35).outerRadius(radius * 0.9);

    // function to compute the pie slice angles
    let pie = d3.layout.pie().sort(null).value(function (d) { return d.portion; });

    // key function
    let key = function (d) { return d.data.product_group; };

    // pie transform
    piesvg.attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");

    // color function
    let color = d3.scale.ordinal()
        .domain(['Eines', 'Kasvikset ja vihannekset', 'Kahvi ja tee', 'Mehukeitot ja muut juomat', 'Lihatuotteet', 'Mausteet',
            'Maitotuotteet', 'Muu', 'Siemenet ja pahkinat', 'Viljatuotteet'])
        .range([col1, col2, col3, col4, col5, col6, col7, col8, col9, col10]);

    // sort order link
    let sortOrder = d3.scale.ordinal()
        .domain(['Tuote', 'Hinta (eur)', 'Paino (kg)', 'Kilohinta (eur/kg)'])
        .range(['product_class', 'total_price', 'total_weight', 'price_per_weight']);


    // ============== PRODUCT VIEW =======================
    // header and table for product list
    let proFrame = d3.select(prodID).append("div").attr('id', 'pro-frame');
    let proTitle = d3.select('#pro-frame').append("h2").attr('id', 'pro-title');
    let proTable = d3.select('#pro-frame').append("table").attr('class', 'pro-list');
    let proHead = proTable.append("thead");
    let proHeadTr = proHead.append('tr');
    let proBody = proTable.append("tbody");

    // append header for product list
    d3.selectAll("#pro-title").text('Tuotteet');

    // histogram dim
    let hisDim = { top: 60, right: 0, bottom: 30, left: 0 };
    hisDim.w = (isMobile ? 0.8 * window.screen.width : 800) - hisDim.left - hisDim.right,
        hisDim.h = (isMobile ? 250 : 500) - hisDim.top - hisDim.bottom;


    // div and svg for histogram
    let hisFrame = d3.select(prodID).append("div").attr('id', 'histogram-frame');
    let hissvg = d3.select('#histogram-frame').append("svg")
        .attr("width", hisDim.w + hisDim.left + hisDim.right)
        .attr("height", hisDim.h + hisDim.top + hisDim.bottom)
        .append("g")
        .attr("transform", "translate(" + hisDim.left + "," + hisDim.top + ")");

    // INIT DATA
    updateOverview()

    // datepickers
    $('#start-date').datepicker({
        dateFormat: 'd.m.yy',
        onSelect: function () {
            $(this).change();
        }
    });
    $('#end-date').datepicker({
        dateFormat: 'd.m.yy',
        onSelect: function () {
            $(this).change();
        }
    });

    // listeners
    $(document).on('change', '#start-date', updateOverview);
    $(document).on('change', '#end-date', updateOverview);

    // get dates and update data
    function updateOverview() {
        let startVal = $('#start-date').val();
        let endVal = $('#end-date').val();
        $.getJSON({
            url: "/get_ruoka",
            data: { start_date: startVal, end_date: endVal },
            success: function (data) {
                // errors to console
                if (data.error.length > 0) {
                    console.log(data.error)
                }
                else {
                    // update overview sum
                    updateOverviewSum(data.total_sum);
                    // update data
                    updateData(data.portions, data.products);
                }
            }
        });
    }

    // function to update overview sum
    function updateOverviewSum(IncomingData) {
        let overCosts = document.getElementById('overview-cost');
        overCosts.innerHTML = IncomingData.toFixed(2) + " &euro;";
    }

    // function to update data
    function updateData(incomingDataPie, incomingDataList) {

        // DATA
        newDataPie = incomingDataPie.map(function (item) {
            return {
                product_group: item.product_group,
                total_price: item.total_price,
                portion: item.portion
            };
        });
        newDataList = incomingDataList.map(function (item) {
            return {
                product_group: item.product_group,
                product_class: item.product_class,
                total_price: item.total_price,
                total_weight: item.total_weight,
                price_per_weight: item.price_per_weight
            };
        });

        // PIE SLICES
        let slice = piesvg.select(".slices").selectAll("path.slice").data(pie(newDataPie), key);

        // append slices
        slice.enter()
            .insert("path")
            .style("fill", function (d) { return color(d.data.product_group); })
            .attr("class", "slice")
            .attr("link", function (d) {
                return key(d);
            })
            .attr("active", "off");

        // transition
        slice
            .transition().duration(500)
            .attrTween("d", function (d) {
                this._current = this._current || d;
                var interpolate = d3.interpolate(this._current, d);
                this._current = interpolate(0);
                return function (t) {
                    return arc(interpolate(t));
                };
            });

        // remove
        slice.exit()
            .remove();

        // PIE LABELS
        let text = piesvg.select(".labels").selectAll("g").data(pie(newDataPie), key);

        // append "g" base for rect and text
        let textLabel = text.enter().append("g")
            .attr("class", "label")
            .attr("link", function (d) {
                return key(d);
            });

        // append text
        textLabel
            .append("text")
            .text(function (d) { return key(d); })
            .style("font-size", "16px")
            .style("font-weight", "bold");

        // append rect under text
        textLabel
            .insert("rect", "text")
            .style("fill", "#FFFFFF");

        // TRANSITIONS
        // update text and rect position
        text.transition().duration(500)
            .attr("transform", function (d) {
                return "translate(" +
                    arc.centroid(d) + ")";
            });

        // update text data, text anchor and add box coordinates to data
        piesvg.selectAll("g").select("text")
            .style("text-anchor", function (d) { return (d.startAngle + d.endAngle) / 2 <= Math.PI ? "end" : "start"; })
            .call(getTextBox);

        // function to return label box coordinates
        function getTextBox(selection) {
            selection.each(function (d) { d.bbox = this.getBBox() });
        }

        // update rect data and rect coordinates
        piesvg.selectAll("g").select("rect")
            .attr("x", function (d) { return d.bbox.x - xAdd })
            .attr("y", function (d) { return d.bbox.y - yAdd })
            .attr("width", function (d) { return d.bbox.width + 2 * xAdd })
            .attr("height", function (d) { return d.bbox.height + 2 * yAdd });

        // remove
        text.exit()
            .remove();

        // PIE FUNCTIONALITY 
        slice
            .on("mouseover", pieMouseOver)
            .on("mouseout", pieMouseOut)
            .on("click", pieClick);
        text
            .on("mouseover", pieMouseOver)
            .on("mouseout", pieMouseOut)
            .on("click", pieClick);

        // function for slice and label action
        function pieLabelAction(attribute, state) {
            // get selected slice, label and table rows
            let selectedSlice = '[class= "slice"][link="' + attribute + '"]';
            let selectedLabel = '[class= "label"][link="' + attribute + '"]';
            let selectedCols = '[class="groupCol"][link="' + attribute + '"],' +
                '[class="portionCol"][link="' + attribute + '"],' +
                '[class="priceCol"][link="' + attribute + '"]'
            // conclude new style from given state
            let newOpacity = (state == 1) ? 0.25 : 1;
            let newDisplay = (state == 1) ? "inline" : "none";
            let newColor = (state == 1) ? function (d) { return color(d.product_group); } : '#FFFFFF';
            // animate ONLY if not selected
            if (d3.select(selectedSlice).attr("active") == "off") {
                d3.select(selectedSlice).transition().duration(200)
                    .style("opacity", newOpacity);
                d3.select(selectedLabel).transition().duration(200)
                    .style("display", newDisplay);
                d3.selectAll(selectedCols)
                    .style("background-color", newColor);
                // expand animation?
            }
        }

        // PIE MOUSEOVER
        function pieMouseOver(d) {
            pieLabelAction(this.getAttribute("link"), 1);
        }

        // PIE MOUSEOUT
        function pieMouseOut(d) {
            pieLabelAction(this.getAttribute("link"), 0);
        }

        // PIE CLICK
        function pieClick(d) {
            // slice
            let selectedSlice = '[class= "slice"][link="' + this.getAttribute("link") + '"]';
            // changed active state
            let active = (d3.select(selectedSlice).attr("active") == "off") ? "on" : "off";
            // CLICKED FIRST TIME
            if (active == "on") {
                // animation
                pieLabelAction(this.getAttribute("link"), 1);
                // change state
                d3.select(selectedSlice).attr("active", active);
                // add to groupList
                groupList.push(d3.select(selectedSlice).attr("link"));
            }
            // CLICKED SECOND TIME
            else {
                // change state
                d3.select(selectedSlice).attr("active", active);
                // animation
                pieLabelAction(this.getAttribute("link"), 0);
                // remove from groupList
                let index = groupList.indexOf(d3.select(selectedSlice).attr("link"));
                if (index > -1) {
                    groupList.splice(index, 1);
                }
            }
            // update selected products
            updateSelectedProduct(groupList, '')

        }

        // update top-list overall view and chosen products list
        updateList();
        updateSelectedProduct(groupList, '');

        // function to update overview list
        function updateList() {
            // order by osuus
            let dataPieSorted = newDataPie.sort(function (a, b) {
                if (a.portion < b.portion) return 1;
                if (a.portion > b.portion) return -1;
                return 0;
            })
            // table rows
            let rows = topBody.selectAll("tr").data(dataPieSorted);
            // new rows
            let rowsEnter = rows.enter().append('tr');
            // tuoteryhma column
            rowsEnter.append("td")
                .attr("class", "groupCol")
                .on("mouseover", pieMouseOver)
                .on("mouseout", pieMouseOut)
                .on("click", pieClick);
            // portion column 
            rowsEnter.append("td")
                .attr("class", "portionCol")
                .on("mouseover", pieMouseOver)
                .on("mouseout", pieMouseOut)
                .on("click", pieClick);
            // price column 
            rowsEnter.append("td")
                .attr("class", "priceCol")
                .on("mouseover", pieMouseOver)
                .on("mouseout", pieMouseOut)
                .on("click", pieClick);

            // update column values
            d3.selectAll(".groupCol").data(dataPieSorted)
                .text(function (d) { return d.product_group; })
                .attr("link", function (d) { return d.product_group; });

            d3.selectAll(".portionCol").data(dataPieSorted)
                .text(function (d) { return Math.round(100 * d.portion, 3); })
                .attr("link", function (d) { return d.product_group; });

            d3.selectAll(".priceCol").data(dataPieSorted)
                .text(function (d) { return d.total_price.toFixed(2); })
                .attr("link", function (d) { return d.product_group; });

            // remove
            rows.exit().remove();

        }

        // function to show selected product groups data
        function updateSelectedProduct(groupList, sortColumn) {
            // init
            let dataGroup = {}, dataGroupSorted = {}

            // ==== FILTER
            // if no group given - show all
            if (groupList.length == 0) {
                dataGroup = newDataList
            }
            // else filter by group
            else {
                dataGroup = newDataList.filter(function (d) {
                    return groupList.indexOf(d.product_group) > -1;
                });
            }

            // ==== SORT
            // if no sort column given - order by group and total price
            if (sortColumn == '') {
                dataGroupSorted = dataGroup.sort(function (a, b) {
                    if (a.product_group.toLowerCase() < b.product_group.toLowerCase()) return 1;
                    if (a.product_group.toLowerCase() > b.product_group.toLowerCase()) return -1;
                    if (a.total_price < b.total_price) return 1;
                    if (a.total_price > b.total_price) return -1;
                    return 0;
                })
            }
            // else sort by given column
            else {
                // if sort by luokka - alphabetical sort
                if (sortColumn == 'product_class') {
                    dataGroupSorted = dataGroup.sort(function (a, b) {
                        if (a[sortColumn] > b[sortColumn]) return 1;
                        if (a[sortColumn] < b[sortColumn]) return -1;
                        return 0;
                    })

                }
                // else - sort from largest to smallest
                else {
                    dataGroupSorted = dataGroup.sort(function (a, b) {
                        if (a[sortColumn] < b[sortColumn]) return 1;
                        if (a[sortColumn] > b[sortColumn]) return -1;
                        return 0;
                    })
                }
            }

            // append the table header row
            let proHeadRow = proHeadTr.selectAll('th').data(['Tuote', 'Hinta (eur)', 'Paino (kg)', 'Kilohinta (eur/kg)']);

            proHeadRow.enter()
                .append('th')
                .text(function (d) { return d; })
                .attr('id', function (d) { return 'id-' + sortOrder(d); })
                .on("click", productHeadClick);
            // table rows
            let proRows = proBody.selectAll("tr").data(dataGroupSorted);
            // new rows
            let proRowsEnter = proRows.enter().append('tr');
            // tuoteryhma column
            proRowsEnter.append("td")
                .attr("class", "productCol")
                .on("mouseover", productMouseOver)
                .on("mouseout", productMouseOut)
                .on("click", productClick);
            // portion column 
            proRowsEnter.append("td")
                .attr("class", "productPriceCol")
                .on("mouseover", productMouseOver)
                .on("mouseout", productMouseOut)
                .on("click", productClick);
            // price column 
            proRowsEnter.append("td")
                .attr("class", "productWeightCol")
                .on("mouseover", productMouseOver)
                .on("mouseout", productMouseOut)
                .on("click", productClick);
            // price per weight column 
            proRowsEnter.append("td")
                .attr("class", "productPricePerWeightCol")
                .text(function (d) { return d.price_per_weight; })
                .on("mouseover", productMouseOver)
                .on("mouseout", productMouseOut)
                .on("click", productClick);

            // update column values
            d3.selectAll(".productCol").data(dataGroupSorted)
                .text(function (d) { return d.product_class; })
                .attr("link", function (d) { return d.product_class; })
                .attr("active", function (d) { return (selectedProd === d.product_class) ? "on" : "off" });

            d3.selectAll(".productPriceCol").data(dataGroupSorted)
                .text(function (d) { return d.total_price.toFixed(2); })
                .attr("link", function (d) { return d.product_class; });

            d3.selectAll(".productWeightCol").data(dataGroupSorted)
                .text(function (d) { return d.total_weight.toFixed(2); })
                .attr("link", function (d) { return d.product_class; });

            d3.selectAll(".productPricePerWeightCol").data(dataGroupSorted)
                .text(function (d) { return d.price_per_weight.toFixed(2); })
                .attr("link", function (d) { return d.product_class; });

            // remove
            proRows.exit().remove();

            // update the row style of all rows
            dataGroupSorted.map(function (d) { productListAction(d.product_class, 0) });

        }

        // function for tuotelista row style
        function productListAction(attribute, state) {
            // selected row
            let selectedCols = '[class="productCol"][link="' + attribute + '"],' +
                '[class="productPriceCol"][link="' + attribute + '"],' +
                '[class="productWeightCol"][link="' + attribute + '"],' +
                '[class="productPricePerWeightCol"][link="' + attribute + '"]'
            // column with active attribute
            let selectedCol = '[class="productCol"][link="' + attribute + '"]'
            // active state
            let activeState = d3.select(selectedCol).attr("active");

            // if active - color according to tuote (no matter the state)
            if (activeState == "on") {
                d3.selectAll(selectedCols).style("background-color", function (d) { return color(d.product_group); });
            }
            // if deactive - color according to state
            else {
                // if selected - color according to tuote
                if (state == 1) {
                    d3.selectAll(selectedCols).style("background-color", function (d) { return color(d.product_group); });
                }
                // else background color
                else {
                    d3.selectAll(selectedCols).style("background-color", '#FFFFFF');
                }
            }
        }

        // TUOTELISTA MOUSEOVER
        function productMouseOver(d) {
            productListAction(this.getAttribute("link"), 1);
        }

        // TUOTELISTA MOUSEOUT
        function productMouseOut(d) {
            productListAction(this.getAttribute("link"), 0);
        }

        // TUOTELISTA CLICK
        function productClick(d) {
            // new chosen row
            let selectedRow = '[class="productCol"][link="' + this.getAttribute("link") + '"]'

            // if product clicked second time - deselect
            if (selectedProd === this.getAttribute("link")) {
                // null global selectedRow
                selectedProd = null;
                // dectivate
                d3.select(selectedRow).attr("active", "off");
                // change style of row
                productListAction(this.getAttribute("link"), 0);
            }
            // else select and update
            else {
                // update global selectedRow
                selectedProd = this.getAttribute("link");
                // previously chosen row
                let previousRow = '[class="productCol"][active="on"]'
                // handle previous row - if there were one
                if (d3.selectAll(previousRow)[0].length > 0) {
                    // link of row
                    let link = d3.selectAll(previousRow)[0][0].getAttribute("link");
                    // change active state of previous row
                    d3.selectAll(previousRow)[0][0].setAttribute("active", "off");
                    // change style of row
                    productListAction(link, 0);
                }
                // new active state
                let active = (d3.select(selectedRow).attr("active") == "off") ? "on" : "off";
                // change style of row
                productListAction(this.getAttribute("link"), 1);
                // update active
                d3.select(selectedRow).attr("active", active);
            }
            // aikasarja, ainoastaan yksi valittuna!
        }

        // function to update product list sort
        function productHeadClick() {
            // get the sort column from header cell id
            let sortAttr = this.id.substring(3, 20);
            // update list order
            updateSelectedProduct(groupList, sortAttr);
        }


        function timeSeries() {
            // HISTOGRAM

            // time series histogram x axis
            let startDay = $('#start-date').val();
            let endDay = $('#end-date').val();

            let x = d3.time.scale()
                .domain([timeFormat.parse(startDay), timeFormat.parse(endDay)])
                .range([20, hisDim.w - 20]);

            hissvg.append("g").attr("class", "x axis")
                .attr("transform", "translate(0," + hisDim.h + ")")
                .call(d3.svg.axis()
                    .scale(x)
                    .orient("bottom")
                    .tickFormat(d3.time.format("%d.%m")));
            /*
            var y = d3.scale.linear()
                .range([hisDim.h, 0])
                .domain([0, d3.max(fD, function (d) { return d[1]; })]);*/
        }

    }
}


