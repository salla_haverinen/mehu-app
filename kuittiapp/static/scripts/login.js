function onBlur(input) {
    if (input.value == '') {
        input.value = input.defaultValue;
    }
}
function onFocus(input) {
    if (input.value == input.defaultValue) {
        input.value = '';
    }
}

function onClick(input) {
    if (input.value == input.defaultValue) {
        input.value = '';
    }
}