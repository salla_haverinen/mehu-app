(Under construction)
# Mehu App
Mehu App (juice app) is my bookkeeping tool and freetime project. Its main components are
- Grocery store receipt reader
- mySQL database
- Flask server
- Web UI

The functionality and the structure of the program are described in more detail below. The purpose of this project is to automate my personal bookkeeping. I use this tool weekly and most of the time I use it on my phone.

## Usage
Execute tests. Use prod flag to exec tests in production environment.
```
python -m pytest tests (--prod)
```

Start application. The default environment is development.
```
python run.py (-env [dev|prod])
```

## Functionality
### Receipt reader
![Reader example](https://78.media.tumblr.com/c1b051a6d57270db24ed510a477b73d9/tumblr_p6cv9aaJEt1r8bn2xo1_500.gif)

Receipts can be uploaded to server via web UI (see route */kuitti* in [reporting.py](/kuittiapp/reporting.py)). Once uploaded, the receipt is read and the output is displayed under the preview file (see gif above). The user can make changes, update data ('Päivitä'), add a new product row ('Lisää rivi') or delete existing row (the 'X' in the last column). When the user is satisfied with the data, they can save the data to database ('Tallenna'). After clicking 'Tallenna', the program tells the user how many rows where added to the database.

The source code of the receipt reader can be found in file [reader.py](/kuittiapp/lib/reader.py). The methods *order_points* and *four_point_transform* are from [**pyimagesearch**](https://www.pyimagesearch.com/) tutorials, the sources are included in code. The OCR tool used is [**Tesseract OCR**](https://github.com/tesseract-ocr/tesseract).

Currently the receipt reader can read the receipts of K-ryhmä, S-ryhmä, and Lidl.

### Reporting layer
![Reporting example](https://78.media.tumblr.com/e72b869ea97144061285ab10cb679902/tumblr_p6cs8rhqoC1r8bn2xo1_500.gif)

In the reporting page the user may view purchases of a given timeline. 

The first section 'Yhteenveto' is an overview section. It displays how much money was spent in total and how did the spent money distribute between different product groups. 

In the second section 'Tuotteet' all the purchased products are listed. The user may sort products according to product name, money spent on product, bought weight or price per weight. The user may examine all the products or just products of specific product groups.

## Structure
### Database
The database server used is [MySQL 5.7](https://dev.mysql.com/doc/refman/5.7/en/). The project has separate databases for development (sastest) and production (sasprod). The database tables and procedures can be created with script [database.sql](/resources/database.sql). Database connections and queries are handled in [database.py](/kuittiapp/database.py).

#### Tables
- *ostokset*
	- This table stores all the purchases.
- *luokitus_link*
	- This table has all the product name - product class links. The program uses these links to determine a product class for a purchase.
- *tuote_info*
	- The average weight and price information is stored in this table. The program sets the weight of a product using the information in this table. The content of this table is generated from historical purchase data i.e. from table *ostokset*.
- *kauppa_info*
	- Information (such as address) of the shops.
- *luokitus*
	- The product group definition.
	
The table links are demonstrated in the picture below.

![Database tables](https://78.media.tumblr.com/e5be8d37b345559f64536f1db6e34054/tumblr_p6f4w2zLLX1r8bn2xo1_540.png)

### Config file
Example of config.py
```
class Config:
    APP_NAME = 'myapp'

class DevelopmentConfig(Config):
    ENV = 'test'
    DEBUG = True
    # TESSERACT
    TESSERACT = <path to tesseract>
    WORDLIST = <path to tesseract word list>
    # PICTURE LOCATIONS
    UPLOAD_FOLDER = <path to upload folder>
    ARCHIVE = <path to receipt archive>
    # PRODUCT NAME LOG FILE
    LOKIFILE = <path to product name log file>
    # DATABASE
    DATABASEDUMP = <path to databasedump executable>
    DATABASEBACKUP = <path to database backup folder>
    CONNECTION = <database connection details>
    # ONEDRIVE
    OD_CLIENT_ID = <OneDrive client id>
    OD_CLIENT_SECRET =  <OneDrive client secret>
    OD_REDIRECT_URI =  <OneDrive redirect uri>
    OD_API_URL =  <OneDrive api url>
    OD_SCOPES =  <OneDrive scopes>
    OD_HEADERS =  <OneDrive request headers>
    # APP SECRET KEY
    SECRET_KEY = <secret key>
    # USERS
    USERFILE = <this is where i store my user credentials>
	
class ProductionConfig(Config):
    ENV = 'prod'
    DEBUG = False
    ...
```