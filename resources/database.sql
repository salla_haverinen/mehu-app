CREATE DATABASE sastest;
USE sastest;

CREATE TABLE `ostokset` (
   `aineistopvm` varchar(8) DEFAULT NULL,
   `vuosi` varchar(4) DEFAULT NULL,
   `kuukausi` varchar(2) DEFAULT NULL,
   `paiva` varchar(2) DEFAULT NULL,
   `luokka` varchar(30) DEFAULT NULL,
   `tuote` varchar(250) DEFAULT NULL,
   `paino` decimal(10,4) DEFAULT NULL,
   `hinta` decimal(10,4) DEFAULT NULL,
   `kpl` int(11) DEFAULT NULL,
   `yhtpaino` decimal(10,4) DEFAULT NULL,
   `tothinta` decimal(10,4) DEFAULT NULL,
   `kilohinta` decimal(10,4) DEFAULT NULL,
   `kauppa` varchar(250) DEFAULT NULL,
   `ostaja` varchar(30) DEFAULT NULL,
   `id` int(11) DEFAULT NULL,
   `latauspvm` varchar(8) DEFAULT NULL
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `luokitus` (
   `jako` varchar(50) NOT NULL,
   `tuoteryhma` varchar(50) NOT NULL,
   `ylaluokka` varchar(50) NOT NULL,
   `luokka` varchar(50) NOT NULL
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8

CREATE TABLE `luokitus_link` (
   `luokka` varchar(50) NOT NULL,
   `tuote` varchar(250) NOT NULL,
   PRIMARY KEY (`tuote`)
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8

CREATE TABLE `tuote_info` (
   `tuote` varchar(250) NOT NULL,
   `avgpaino` decimal(10,4) NOT NULL,
   `avghinta` decimal(10,4) NOT NULL
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8

 CREATE TABLE `kauppa_info` (
   `kauppa` varchar(250) NOT NULL,
   `osoite` varchar(250) NOT NULL,
   `kaupunki` varchar(50) NOT NULL,
   `postinumero` int(11) NOT NULL
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8

DELIMITER //
 CREATE DEFINER=`root`@`localhost` PROCEDURE `add_luokitus_link`(
 	IN p_luokka varchar(50),
     IN p_tuote varchar(255)
     )
 BEGIN
 	IF ( NOT EXISTS (SELECT 1 FROM luokitus_link WHERE tuote = lower(p_tuote) ) ) THEN
 		BEGIN
 			INSERT INTO luokitus_link (
 				luokka,
 				tuote)
 			VALUES (
 				lower(p_luokka),
 				lower(p_tuote)
 				);
 		END;
 	END IF;
 END
 //

DELIMITER //
 CREATE DEFINER=`root`@`localhost` PROCEDURE `add_ostos`(
 		IN p_aineistopvm varchar(8),
         IN p_vuosi varchar(4),
         IN p_kuukausi varchar(2),
         IN p_paiva varchar(2),
         IN p_luokka varchar(30),
         IN p_tuote varchar(255),
         IN p_paino decimal(10,4),
         IN p_hinta decimal(10,4),
         IN p_kpl int,
         IN p_yhtpaino decimal(10,4),
         IN p_kilohinta decimal(10,4),
         IN p_yhthinta decimal(10,4),
         IN p_kauppa varchar(255),
         IN p_ostaja varchar(30)
         )
 BEGIN
 	SET @maxid = (SELECT MAX(id) + 1 AS maxid FROM ostokset);
 
 	INSERT INTO ostokset (
 		aineistopvm,
         vuosi,
         kuukausi,
         paiva,
         luokka,
         tuote,
         paino,
         hinta,
         kpl,
         yhtpaino,
         kilohinta,
         tothinta,
         kauppa,
         ostaja,
         latauspvm,
         id)
 	VALUES (
 		p_aineistopvm,
         p_vuosi,
         p_kuukausi,
         p_paiva,
         p_luokka,
         p_tuote,
         p_paino,
         p_hinta,
         p_kpl,
         p_yhtpaino,
         p_kilohinta,
         p_yhthinta,
         p_kauppa,
         p_ostaja,
         DATE_FORMAT(NOW(), '%Y%m%d'),
         @maxid
         );
 END
 //

DELIMITER //
 CREATE DEFINER=`root`@`localhost` PROCEDURE `add_tuote_info`(
 	IN p_tuote varchar(255),
     IN p_avgpaino decimal(10,4),
     IN p_avghinta decimal(10,4)
     )
 BEGIN
 	INSERT INTO tuote_info (tuote,avgpaino,avghinta) VALUES (p_tuote,p_avgpaino,p_avghinta);
 END
 //

DELIMITER //
 CREATE DEFINER=`root`@`localhost` PROCEDURE `clean_test_luokitus_link`()
 BEGIN
 delete from luokitus_link where luokka = 'testi' and tuote = 'testi';
 END;
 //

DELIMITER //
 CREATE DEFINER=`root`@`localhost` PROCEDURE `clear_tuote_info`()
 BEGIN
 	truncate table tuote_info;
 END
 //
