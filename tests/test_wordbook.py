from kuittiapp.database import database
from kuittiapp.lib import wordbook
from kuittiapp.lib.product import product

def test_update_word_list(word_list_path):
    product_list = []
    product_list.append(product('20000101', '2000', '01', '01', 'maito', 'OIVARIINI VÄHEMMAN', 0, 3.15, 1, 0, 3.15, 0, 'Prisma Viikki', 'Salla'))
    product_list.append(product('20150101', 2015, 1, 1, 'testi', 'CLASSIC FESTIVITA', 0, 4.95, 1, 0, 4.95, 0, 'Prisma Viikki', 'Salla'))
    product_list.append(product('20150101', 2015, 1, 1, 'testi', 'LOIMULOHIFPALA', 0, 4.95, 1, 0, 4.95, 0, 'Prisma Viikki', 'Salla'))
    # update word list
    added = wordbook.update_word_list(product_list, word_list_path)
    # test nothing added
    assert added == 0
    # test that the elements of word list are unique
    with open(word_list_path,'r') as word_list:
        read_word_list = word_list.read().splitlines()
    instance_count = [read_word_list.count(x) for x in read_word_list]
    max_kpl = max(instance_count)
    assert max_kpl == 1
    
def test_fix_product_names(database_str, word_list_path):
    cnc = database(database_str)
    cnc.load_average_info()    
    # test data
    product_list = []
    product_list.append(product('20150101', 2015, 1, 1, 'testi', 'CLASSIC FESTIVITA SL', 0, 4.95, 1, 0, 4.95, 0, 'Prisma Viikki', 'Salla'))
    product_list.append(product('20150101', 2015, 1, 1, 'testi', 'CIVARIIP-II VAHEMMAN SUOLAA', 0, 4.95, 1, 0, 4.95, 0, 'Prisma Viikki', 'Salla'))
    product_list.append(product('20150101', 2015, 1, 1, 'testi', 'ISOT KANNJMUNAT 15 KPL', 0, 4.95, 1, 0, 4.95, 0, 'Prisma Viikki', 'Salla'))
    product_list.append(product('20150101', 2015, 1, 1, 'testi', 'BANAANI LUUMU', 0, 4.95, 1, 0, 4.95, 0, 'Prisma Viikki', 'Salla'))
    product_list.append(product('20000101', '2000', '01', '01', 'maito', 'ISOT KMMMUNAT 15 KPL', 0, 3.15, 1, 0, 3.15, 0, 'Prisma Viikki', 'Salla'))
    product_list.append(product('20150101', 2015, 1, 1, 'testi', 'PORKKMPHSSI', 0, 4.95, 1, 0, 4.95, 0, 'Prisma Viikki', 'Salla'))
    product_list.append(product('20150101', 2015, 1, 1, 'testi', 'RUISPUIKLLAT', 0, 4.95, 1, 0, 4.95, 0, 'Prisma Viikki', 'Salla'))
    product_list.append(product('20150101', 2015, 1, 1, 'testi', 'KAUR-IIUTALE', 0, 4.95, 1, 0, 4.95, 0, 'Prisma Viikki', 'Salla'))
    # fix product names
    wordbook.fix_product_names(product_list, cnc.average_info, word_list_path)
    # test fix
    assert product_list[0].product_name == 'CLASSIC FESTIVITA SJ'
    assert product_list[1].product_name == 'OIVARIINI VAHEMMAN SUOLAA'
    assert product_list[2].product_name == 'ISOT KANANMUNAT 15 KPL'
    assert product_list[3].product_name == 'BANAANI LUOMU'
    assert product_list[4].product_name == 'ISOT KANANMUNAT 15 KPL'
    assert product_list[5].product_name == 'PORKKANAPUSSI'
    assert product_list[6].product_name == 'RUISPUIKULAT'
    assert product_list[7].product_name == 'KAURAHIUTALE'
    # close
    cnc.connection.close()
