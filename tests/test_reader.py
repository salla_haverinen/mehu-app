'''
Tests for reader and scanner.
'''
import cv2
import numpy as np

from kuittiapp.lib import reader

def test_scanner(file_path):
    scanned_image = reader.scan_image(file_path)
    # test type
    assert str(type(scanned_image)) == "<class 'numpy.ndarray'>"

def test_detect_words(file_path):
    read_image = cv2.imread(file_path)
    ratio = 1575/np.size(read_image, 1)
    image = reader.modify_image(read_image, ratio = ratio , gray = 1, blur_filter = 1, blur_gaus = 0, treshold = 1, inverse =0)
    # test S-group
    word_unit_list = reader.detect_words(image, 'S')
    assert len(word_unit_list) > 0


    
