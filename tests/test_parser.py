from kuittiapp.lib import parser

def test_replace_scandinavian():
    string_1 = 'ÄÄÄÄää'
    assert parser.replace_scandinavian(string_1) == 'AAAAaa'
    string_2 = 'Ååh'
    assert parser.replace_scandinavian(string_2) == 'Aah'
    string_3 = 'ÖÖppöö'
    assert parser.replace_scandinavian(string_3) == 'OOppoo'