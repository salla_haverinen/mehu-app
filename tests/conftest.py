import pytest
from os import listdir
from kuittiapp import app

def pytest_addoption(parser):
    '''
    Variable which tells pytest to run tests in production environment.
    '''
    parser.addoption("--prod", action="store_true", help="Run tests in prod")

def pytest_generate_tests(metafunc):
    '''
    Kuvaus
    '''
    # test prod
    if metafunc.config.option.prod:
        app.config.from_object('config.ProductionConfig')
    
   # receipts used in tests
    checks = ['Vanhat/IMG_20180119_091602.jpg','Vanhat/IMG_20180206_161647.jpg', 'Vanhat/IMG_20180112_100907.jpg']

    if 'file_path' in metafunc.fixturenames:
        file_win = app.config['ARCHIVE'].replace("\\", "/")
        test_receipts = list(map(lambda check: file_win + check, checks))
        metafunc.parametrize("file_path", test_receipts)

    if 'word_list_path' in metafunc.fixturenames:
        word_path = app.config['WORDLIST'].replace("\\", "/")
        metafunc.parametrize("word_list_path", [word_path])

    if 'database_str' in metafunc.fixturenames:
        datab = app.config['CONNECTION']
        metafunc.parametrize("database_str", [datab])

    if 'app_config' in metafunc.fixturenames:
        metafunc.parametrize("app_config", [app.config])

    if 'file_list' in metafunc.fixturenames:
        file_win = app.config['ARCHIVE'].replace("\\", "/") + 'L/'
        file_names = [file_win + file_name for file_name in listdir(file_win)]
        metafunc.parametrize("file_list", file_names)

