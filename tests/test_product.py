'''
Unit tests for class product
'''
from kuittiapp.lib.product import product
    
def test_count_total_weight():
    unit = product('01012000', '2000', '01', '01', '', '', 0, 0, 0, 0, 0, 0, 'store', '')
    unit.total_weight = 0
    unit.unit_weight = 0
    unit.units = 1
    unit.count_total_weight()
    assert unit.total_weight == 0

    unit.total_weight = 0
    unit.unit_weight = 0.4
    unit.units = 1
    unit.count_total_weight()
    assert unit.total_weight == 0.4

    unit.total_weight = 0
    unit.unit_weight = 0.4
    unit.units = 3
    unit.count_total_weight()
    assert unit.total_weight == 1.2

    unit.total_weight = 0
    unit.unit_weight = 'apina'
    unit.units = 0
    unit.count_total_weight()
    assert unit.total_weight == 0

    unit.total_weight = 'kissa'
    unit.unit_weight = 0
    unit.units = 3
    unit.count_total_weight()
    assert unit.total_weight == 'kissa'

    unit.total_weight = 'pulla'
    unit.unit_weight = 'apina'
    unit.units = 0
    unit.count_total_weight()
    assert unit.total_weight == 'pulla'

    unit.total_weight = 0
    unit.unit_weight = 5
    unit.units = 2.3
    unit.count_total_weight()
    assert unit.total_weight == 10

    unit.total_weight = 0
    unit.unit_weight = 5
    unit.units = 2.7
    unit.count_total_weight()
    assert unit.total_weight == 10

def test_count_price_per_weight():
    unit = product('01012000', '2000', '01', '01', '', '', 0, 0, 0, 0, 0, 0, 'store', '')
    unit.price_per_weight = 0
    unit.unit_weight = 0
    unit.unit_price = 0
    unit.count_price_per_weight()
    assert unit.price_per_weight == 0

    unit.price_per_weight = 0
    unit.unit_weight = 1
    unit.unit_price = 0.7
    unit.count_price_per_weight()
    assert unit.price_per_weight == 0.7

    unit.price_per_weight = 2
    unit.unit_weight = 1
    unit.unit_price = 0.7
    unit.count_price_per_weight()
    assert unit.price_per_weight == 0.7

    unit.price_per_weight = 'kissa'
    unit.unit_weight = 1
    unit.unit_price = 0
    unit.count_price_per_weight()
    assert unit.price_per_weight == 'kissa'

def test_update_unit_price():
    unit = product('01012000', '2000', '01', '01', '', '', 0, 0, 0, 0, 0, 0, 'store', '')
    unit.total_price = 0
    unit.units = 0
    unit.unit_price = 0
    unit.update_unit_price()
    assert unit.unit_price == 0

    unit.total_price = 0.66
    unit.units = 1
    unit.unit_price = 0
    unit.update_unit_price()
    assert unit.unit_price == 0.66

    unit.total_price = 1
    unit.units = 2
    unit.unit_price = 0
    unit.update_unit_price()
    assert unit.unit_price == 0.5

    unit.total_price = 1
    unit.units = 2
    unit.unit_price = 0.44
    unit.update_unit_price()
    assert unit.unit_price == 0.5

    unit.total_price = 1
    unit.units = 'kumontha'
    unit.unit_price = 0
    unit.update_unit_price()
    assert unit.unit_price == 0

def test_update_purchase_date():
    unit = product('01012000', '2000', '01', '01', '', '', 0, 0, 0, 0, 0, 0, 'store', '')
    unit.date = '11092012'
    unit.day = '00'
    unit.month = '00'
    unit.year = '0000'
    unit.update_purchase_date()
    assert unit.day == 11
    assert unit.month == 9
    assert unit.year == 2012

    unit.date = '20150405'
    unit.day = '00'
    unit.month = '00'
    unit.year = '0000'
    unit.update_purchase_date()
    assert unit.day == 5
    assert unit.month == 4
    assert unit.year == 2015

    unit.date = '03--10/(2016'
    unit.day = '00'
    unit.month = '00'
    unit.year = '0000'
    unit.update_purchase_date()
    assert unit.date == '20161003'
    assert unit.day == 3
    assert unit.month == 10
    assert unit.year == 2016

    unit.date = '2018&0434'
    unit.update_purchase_date()
    assert unit.date  == '2018-0434'

    unit.date = '3-05-2018'
    unit.update_purchase_date()
    assert unit.date == '20180503'

    unit.date = ''
    unit.update_purchase_date()
    assert unit.date == ''

def test_clean_product():
    unit = product(
        '01-01..2000', '2090', '09', '09', 
        'hrje&¤44', 'ostosÄÄääååÅ??', 
        1, 'apina', 2, 'koira', '', 0, 'store**q', '')
    
    unit.clean_product()

    assert unit.date == '01-01..2000'
    assert unit.year == 2090
    assert unit.month == 9
    assert unit.day == 9
    assert unit.product_class == 'hrje&44'
    assert unit.product_name == 'ostosAAaa??'
    assert unit.unit_weight == 1
    assert unit.unit_price == 0
    assert unit.units == 2
    assert unit.total_weight == 0
    assert unit.total_price == 0
    assert unit.price_per_weight == 0
    assert unit.store == 'store**q'
    assert unit.buyer == ''

def test_product_to_dict():
    unit_list = [product('01012000', '2000', '01', '01', '', 'ostos ' + str(i), 0, 0, 0, 0, 0, 0, 'store', '') for i in range(5)]
    # product wrap
    unit_dict_list = list(map(lambda x: x.product_to_dict(), unit_list))
    # length test
    assert len(unit_dict_list) == 5
    # type test
    assert [type(unit_dict_list[i]) for i in range(5)] == [dict] * 5
    # content test
    assert unit_dict_list == [{
        'date':'01012000', 
        'year':2000, 
        'month':1, 
        'day':1, 
        'product_class':'', 
        'product_name':'ostos ' + str(i), 
        'unit_weight':0,
        'unit_price':0, 
        'units':0, 
        'total_weight':0, 
        'total_price':0, 
        'price_per_weight':0, 
        'store':'store',
        'buyer':''
        } for i in range(5)]

def test_dict_to_product():
    product_dict = {
        'date':'01012000', 
        'year':2000, 
        'month':1, 
        'day':1, 
        'product_class':'', 
        'product_name':'ostos 1', 
        'unit_weight':0,
        'unit_price':0, 
        'units':0, 
        'total_weight':0, 
        'total_price':0, 
        'price_per_weight':0, 
        'store':'store',
        'buyer':''
        }

    unit = product.dict_to_product(product_dict)
    assert unit.product_name == 'ostos 1'
    assert unit.product_class == ''


