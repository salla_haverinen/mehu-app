'''
Tests for database tools.
'''
from kuittiapp import database, reporting_methods
from kuittiapp.lib.product import product
from decimal import *
import pytest
 
def test_init(database_str):
    cnc = database.database(database_str)
    # test init
    assert cnc.product_classes == {}
    assert cnc.average_info == []
    # close
    cnc.connection.close()

def test_load_average_info(database_str):
    cnc = database.database(database_str)
    cnc.load_average_info()
    # test average info collected and data type
    assert len(cnc.average_info) > 0
    assert type(cnc.average_info) == list
    # test few cases
    tee = [info for info in cnc.average_info if info['product_name'] == 'tee']
    assert len(tee) == 1
    oivariini = [info for info in cnc.average_info if info['product_name'] == 'OIVARIINI VAHEMMAN SUOLAA']
    assert len(oivariini) == 2
    # close
    cnc.connection.close()

def test_load_product_classes(database_str):
    cnc = database.database(database_str)
    cnc.load_product_classes()
    # test classes collected and data type
    assert len(cnc.product_classes) > 0
    assert type(cnc.product_classes) == dict
    # test few cases
    assert cnc.product_classes['valkosipuli'] == 'valkosipuli'
    assert cnc.product_classes['FUSILLI KUVIOPASTA'] == 'fusilli'
    # test keys unique
    key_list = list(cnc.product_classes.keys())
    instance_count = [key_list.count(x) for x in key_list]
    assert max(instance_count) == 1
    # close
    cnc.connection.close()

def test_set(database_str):
    product_list = []
    product_list.append(product('20000101', '2000', '01', '01', '', 'KEVYTMAITO', 0, 0.85, 1, 0, 0.85, 0, 'Prisma Viikki', 'Salla'))
    product_list.append(product('20000101', '2000', '01', '01', 'maito', 'OIVARIINI VAHEMMAN SUOLAA', 0, 3.15, 1, 0, 3.15, 0, 'Prisma Viikki', 'Salla'))
    product_list.append(product('20000101', '2000', '01', '01', '', 'Kevytviili', 0, 0.28, 2, 0, 0.56, 0, 'Prisma Viikki', 'Salla'))
    product_list.append(product('20000101', '2000', '01', '01', '', 'VALKOSIPULI', 0, 0.28, 2, 0, 0.56, 0, 'Prisma Viikki', 'Salla'))
    # set weights and classes
    cnc = database.database(database_str)
    cnc.load_product_classes()
    cnc.load_average_info()
    for unit in product_list:
        cnc.set_product_class(unit)
        cnc.set_product_weigh(unit)
    # test classes
    assert product_list[0].product_class == 'maito'
    assert product_list[1].product_class == 'oivariini'
    assert product_list[2].product_class == 'viili'
    assert product_list[3].product_class == 'valkosipuli'
    # test weights
    assert product_list[0].unit_weight == 1
    assert product_list[1].unit_weight == 0.55
    assert product_list[2].unit_weight == 0.2
    # close
    cnc.connection.close()

def test_add_purchase(database_str):
    cnc = database.database(database_str)
    cursor = cnc.connection.cursor()
    select_string = "select max(id) as id from ostokset;"
    cursor.execute(select_string)
    id_before = cursor.fetchone().id
    # test 3 products added
    product_list = []
    product_list.append(product('20111206', 2011, 12, 6, 'testi', 'KEVYTMAITO', 0, 0.85, 1, 0, 0.85, 0, 'Prisma Viikki', 'Salla'))
    product_list.append(product('20160105', 2016, 1, 5, 'testi', 'OIVARIINI VAHEMMAN SUOLAA', 0, 3.15, 1, 0, 3.15, 0, 'Prisma Viikki', 'Salla'))
    product_list.append(product('20150101', 2015, 1, 1, 'testi', 'Kevytviili', 0, 0.28, 2, 0, 0.56, 0, 'Prisma Viikki', 'Salla'))
    for unit in product_list:
        cnc.add_purchase(unit)
    cursor.execute(select_string)
    id_after = cursor.fetchone().id
    assert id_after - id_before == 3
    # test content of 3 last rows from database
    cursor = cnc.connection.cursor()
    exec_string = 'select * from ostokset order by id desc LIMIT 3;'
    cursor.execute(exec_string)
    rows = cursor.fetchall()
    assert rows[0][:-2] == ('20150101', '2015', '1', '1', 'testi', 'Kevytviili', Decimal('0.0000'), Decimal('0.2800'), 2, Decimal('0.0000'), Decimal('0.5600'), Decimal('0.0000'), 'Prisma Viikki', 'Salla')
    assert rows[1][:-2] == ('20160105', '2016', '1', '5', 'testi', 'OIVARIINI VAHEMMAN SUOLAA', Decimal('0.0000'), Decimal('3.1500'), 1, Decimal('0.0000'), Decimal('3.1500'), Decimal('0.0000'), 'Prisma Viikki', 'Salla')
    assert rows[2][:-2] == ('20111206', '2011', '12', '6', 'testi', 'KEVYTMAITO', Decimal('0.0000'), Decimal('0.8500'), 1, Decimal('0.0000'), Decimal('0.8500'), Decimal('0.0000'), 'Prisma Viikki', 'Salla')
    # clean and close
    select_string = "delete from ostokset where luokka = 'testi';"
    cursor.execute(select_string)
    cursor.commit()
    cursor.close()
    cnc.connection.close()

def test_update_class_links_table(database_str):
    cnc = database.database(database_str)
    cursor = cnc.connection.cursor()
    select_string = "select luokka as product_class, tuote as product_name from luokitus_link where tuote like'{}';".format('testi')
    cursor.execute(select_string)
    db_rows_before = len(cursor.fetchall())
    unit = product('20150101', 2015, 1, 1, 'testi', 'KEVYTMAITO', 0, 0.85, 1, 0, 0.85, 0, 'Prisma Viikki', 'Salla')
    # test nothing is added
    cnc.update_class_links_table(unit)
    cursor.execute(select_string)
    db_rows_after = len(cursor.fetchall())
    assert abs(db_rows_before - db_rows_after) == 0
    # test one is added
    unit = product('20150101', 2015, 1, 1, 'testi', 'testi', 0, 3.15, 1, 0, 3.15, 0, 'Prisma Viikki', 'Salla')
    cnc.update_class_links_table(unit)
    cursor.execute(select_string)
    db_rows_after = len(cursor.fetchall())
    assert abs(db_rows_before - db_rows_after) == 1
    # clean and close
    exec_string = "delete from luokitus_link where luokka = 'testi';"
    cursor.execute(exec_string)
    cursor.commit()
    cursor.close()
    cnc.connection.close()

def test_update_average_info_table(database_str):
    cnc = database.database(database_str)
    cnc.update_average_info_table()
    # test all products have info row
    cursor = cnc.connection.cursor()
    select_string_1 = "select distinct(tuote) as name from ostokset where luokka not in ('testi');"
    cursor.execute(select_string_1)
    products = cursor.fetchall()
    sorted_products = sorted([x.name.lower() for x in products])
    select_string_2 = "select distinct(tuote) as name from tuote_info;"
    cursor.execute(select_string_2)
    information = cursor.fetchall()
    sorted_information = sorted([x.name.lower() for x in information])
    cursor.close()
    assert sorted_products == sorted_information
    # close
    cnc.connection.close()

def test_date_handler():
    date = database.database.date_handler('1.1.2018')
    assert date == '20180101'
    date = database.database.date_handler('2.2.2018')
    assert date == '20180202'
    # test wrong format
    date = database.database.date_handler('3.2.2018')
    assert date == '20180203'
    # test wrong format
    date = database.database.date_handler('11.5.2015')
    assert date == '20150511'
    # test wrong format
    date = database.database.date_handler('2013.4.06')
    assert date == None
    # test not date
    date = database.database.date_handler('kissatonje')
    assert date == None

def test_get_timeline_food_purchases(database_str):
    cnc = database.database(database_str)
    # test wrong date raises error
    with pytest.raises(ValueError):
        cnc.get_timeline_food_purchases('1.16.2018', '02.02.2018')
    # test wrong date order raises error
    with pytest.raises(ValueError):
        cnc.get_timeline_food_purchases('03.12.2017', '05.10.2017')
    # test data fetch
    data = cnc.get_timeline_food_purchases('01.01.2018', '17.09.2018')
    assert len(data) > 0
    # test type
    assert str(type(data)) == "<class 'pandas.core.frame.DataFrame'>"
    # close
    cnc.connection.close()

def test_get_historical_purchases(database_str):
    cnc = database.database(database_str)
    # test data fetch
    data = cnc.get_historical_purchases()
    assert len(data) > 0
    # test type
    assert type(data) == list
    # close
    cnc.connection.close()

# reporting_methods tests
def test_ruoka_get_data(database_str):
    cnc = database.database(database_str)
    # test wrong date raises error
    with pytest.raises(ValueError):
        reporting_methods.ruoka_get_data('02.5.2016', '03.78.2017', cnc)
    # test wrong date order raises error
    with pytest.raises(ValueError):
        reporting_methods.ruoka_get_data('02.05.2018', '03.07.2017', cnc)
    # test data fetch
    sum_data, pie_data, product_data = reporting_methods.ruoka_get_data('01.01.2016', '03.07.2018', cnc)
    assert sum_data > 0
    assert len(pie_data) > 0
    assert [type(pie_data[i]) for i in range(len(pie_data))] == [dict] * len(pie_data)
    assert len(product_data) > 0
    assert [type(product_data[i]) for i in range(len(product_data))] == [dict] * len(product_data)
    # close
    cnc.connection.close()

def test_kulut_get_data(database_str):
    cnc = database.database(database_str)
    # test error cases
    history_data = reporting_methods.kulut_get_data(cnc)
    assert len(history_data) >0
    cnc.connection.close()

def test_update_product(database_str):
    cnc = database.database(database_str)
    cnc.load_product_classes()
    cnc.load_average_info()
    # test data
    data = [{'date': '9-01-2018', 'year': '2000', 'month': '01', 'day': '01', 'product_class': '', 'product_name': 'SUIIN<ASTIKE', 'unit_weight': 0, 'unit_price': 0, 'units': 1, 'total_weight': 0, 'total_price': 4.99, 'price_per_weight': 0, 'buyer': '', 'store': 'ALEPA KOS¤¤KELA'}, 
    {'date': '9-01-2018', 'year': '2000', 'month': '01', 'day': '01', 'product_class': 'vanukas', 'product_name': 'TUPLASUKLAAVANUKAS', 'unit_weight': 0.2, 'unit_price': 0.89, 'units': 2, 'total_weight': 0, 'total_price': 1.78, 'price_per_weight': 0, 'buyer': '', 'store': 'ALEPA KOSKELA'}, 
    {'date': '9-01-2018', 'year': '2000', 'month': '01', 'day': '01', 'product_class': '', 'product_name': 'KEVYTVIILI', 'unit_weight': 0, 'unit_price': 0,'units': 2, 'total_weight': 0, 'total_price': 0.8, 'price_per_weight': 0, 'buyer': '', 'store': 'ALEPA KOSKELA'}, 
    {'date': '9-01-2018', 'year': '2000', 'month': '01', 'day': '01', 'product_class': 'baby', 'product_name': 'PERSIKKA-BANAANI 4KK', 'unit_weight': 0, 'unit_price': 0, 'units': 3, 'total_weight': 0, 'total_price': 1.38, 'price_per_weight': 0, 'buyer': '', 'store': 'ALEPA KOSÅÅKELA'}, 
    {'date': '9-01-2018', 'year': '2000', 'month': '01', 'day': '01', 'product_class': 'kinkku', 'product_name': 'KOTASAVU PALVISUIKALE', 'unit_weight': 0, 'unit_price': 0, 'units': 1, 'total_weight': 0, 'total_price': 1.95, 'price_per_weight': 0, 'buyer': 'ÖÖ', 'store': 'ALEPA KOSKELA'}]
    result_list, c_sum = reporting_methods.update_receipt(data, cnc)
    # test type
    assert len(result_list) == 5
    assert [type(result_list[i]) for i in range(5)] == [dict] * 5
    # test content
    assert result_list[0] == {'date': '20180109', 'year': 2018, 'month': 1, 'day': 9, 'product_class': '', 'product_name': 'SUIIN<ASTIKE', 'unit_weight': 0, 'unit_price': 4.99, 'units': 1, 'total_weight': 0, 'total_price': 4.99, 'price_per_weight': 0, 'buyer': '', 'store': 'ALEPA KOSKELA'}
    assert result_list[1] == {'date': '20180109', 'year': 2018, 'month': 1, 'day': 9, 'product_class': 'vanukas', 'product_name': 'TUPLASUKLAAVANUKAS', 'unit_weight': 0.2, 'unit_price': 0.89, 'units': 2, 'total_weight': 0.4, 'total_price': 1.78, 'price_per_weight': 4.45, 'buyer': '', 'store': 'ALEPA KOSKELA'}
    assert result_list[2] == {'date': '20180109', 'year': 2018, 'month': 1, 'day': 9, 'product_class': 'viili', 'product_name': 'KEVYTVIILI', 'unit_weight': 0.2, 'unit_price': 0.4, 'units': 2, 'total_weight': 0.4, 'total_price': 0.8, 'price_per_weight': 2, 'buyer': '', 'store': 'ALEPA KOSKELA'}
    assert result_list[3] == {'date': '20180109', 'year': 2018, 'month': 1, 'day': 9, 'product_class': 'baby', 'product_name': 'PERSIKKA-BANAANI 4KK', 'unit_weight': 0.125, 'unit_price': 0.46, 'units': 3, 'total_weight': 0.375, 'total_price': 1.38, 'price_per_weight': 3.68, 'buyer': '', 'store': 'ALEPA KOSKELA'}
    assert result_list[4] == {'date': '20180109', 'year': 2018, 'month': 1, 'day': 9, 'product_class': 'kinkku', 'product_name': 'KOTASAVU PALVISUIKALE', 'unit_weight': 0.23, 'unit_price': 1.95, 'units': 1, 'total_weight': 0.23, 'total_price': 1.95, 'price_per_weight': 8.478, 'buyer': 'OO', 'store': 'ALEPA KOSKELA'}
    assert round(c_sum,1) == 10.9
    # close
    cnc.connection.close()

def test_send_product(database_str, app_config):
    cnc = database.database(database_str)
    data = [{'date': '20000101', 'year': '2000', 'month': '01', 'day': '01', 'product_class': 'testi', 'product_name': 'testi', 'unit_weight': 0, 'unit_price': 0, 'units': 1, 'total_weight': 0, 'total_price': 1, 'price_per_weight': 0, 'buyer': 'testi', 'store': 'Testikauppa'}]
    added_purchases = reporting_methods.send_receipt(data, cnc, app_config)
    assert added_purchases == 1
    cnc.connection.close()

@pytest.mark.last
def test_clean_all(database_str):
    '''
    Clean after tests.
    '''
    cnc = database.database(database_str)
    cursor = cnc.connection.cursor()
    exec_string = "delete from luokitus_link where luokka = 'testi';"
    cursor.execute(exec_string)
    cursor.commit() 
    select_string = "delete from ostokset where luokka = 'testi';"
    cursor.execute(select_string)
    cursor.commit()
    cursor.close()
    cnc.connection.close()



